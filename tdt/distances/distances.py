"""

  Module for computing distances between timeseries.
  Available distances includes cort, dtw, dtw_cort.
  It requires numpy.

"""
# $Id: distancesV1.py 3903 2016-08-22 14:12:33Z fthollard/S.Alkhoury $

import numpy as np
from cffi import FFI
import platform
import sys
import os.path
import tdt.distances.DistanceObject as Do


precision = -0.00000000001


def load_shared():
    """
    Loads a wrapper over the shared library). The shared library is built
    using the command line python3 setup.py build_ext
    The library path should include the path where the shared library is.

    """

    ffi = FFI()
    ffi.cdef("""
    int get_idx(int var_idx, int time_idx, int nbvar);
    double compute_diff(double *x, double *y, int t_x, int t_y, int nbvar);
    void compute_slope(double *x, int start_x, int end_x, int nb_var, double *res);
    void swap_pointers(double **x, double **y);
    void print_multivariate(double *x, int nbvar, int timelen);
    void dtw(double *x, double *y, int size_x, int size_y, int nbvar, double *path, double* distances, int window);
    void dtw_weighted(double *x, double *y, int size_x, int size_y, int nbvar, double* distances, double* weight, int window);
    void dtw_given_path(double* temporal_distances, int* path, double* weight, int path_length, double* distances);
    double cort(double* x, double* y, int start_x, int start_y, int end_x, int end_y, int nbvar, double k);
    void cort_window_path(double* x, double* y, int start_x, int start_y, int end_x, int end_y, int nbvar, double k, double* path, double* distances, int window);
    double cort_dtw(double* x, double* y, int start_x, int start_y, int end_x, int end_y, int nbvar, double k);
    void cort_dtw_window_path(double* x, double* y, int start_x, int start_y, int end_x, int end_y, int nbvar, double k, double* path, double* distances, int window);
    """)

    my_path = os.path.abspath(os.path.dirname(__file__))
    if platform.system() == 'Linux':
        _lib_name = 'distances.cpython-34m.so'
    elif platform.system() == 'Darwin':
        _lib_name = 'distances.cpython-35m-darwin.so'
    else:
        raise RuntimeError('unhandled system {}, cannot load shared library'.format(platform.system()))

    return ffi, ffi.dlopen('/'.join([my_path, _lib_name]))

__ffi, __dllib = load_shared()


def dtw(series_a=None, series_b=None, weight=None, get_path=False, path=None, window=None):

    """
    Computes the dynamic time wrapping distance between (multivariate )series_a and series_b.

    :param series_a: the first series
    :param series_b: the second series
    :param weight: weight vector to use (related to series_a)
    :param get_path: return the path or not
    :param path: Given path to be used in the distance computation
    :param window: window look-ahead for DTW, CorT and CorT_DTW

    :note: The local difference between two points of the time series is the sum of the absolute
    differences of the variables.
    """
    len_a, len_b, nb_var = 0, 0, 0
    a_ptr, b_ptr = None, None
    if series_a is not None:
        nb_var = len(series_a.shape)
        if nb_var > 1:
            series_a = series_a.T
            series_b = series_b.T

        len_a = len(series_a)
        len_b = len(series_b)

        a_ptr = __ffi.cast("double*", series_a.ctypes.data)
        b_ptr = __ffi.cast("double*", series_b.ctypes.data)

        if (len_a == 0) or (len_b == 0):
            return sys.float_info.max

        # maximum potential size of the path

        assert len_a >= 0, "time_start_a should be positive (actual value={})".format(len_a)
        assert len_b >= 0, "time_start_b should be positive (actual value={})".format(len_b)

    dist_obj = Do.DistanceObject()
    temp_window = len(weight) if series_a is None else len(series_a)
    if window is None:
        window = temp_window - 1
    else:
        if 0 < window < 1:
            window = round(temp_window * window) - 1

    if series_a is not None:
        if window < abs(len(series_a) - len(series_b)):
            raise AttributeError('window size is too small to calculate DTW!')

    if window < 0:
        window = 0

    if weight is None:
        all_path = np.zeros((len_a + len_b) * 3, dtype=np.float64)
        distances = np.zeros(3, dtype=np.float64)
        d_ptr = __ffi.cast("double*", distances.ctypes.data)
        p_ptr = __ffi.cast("double*", all_path.ctypes.data)
        __dllib.dtw(a_ptr, b_ptr, len_a, len_b, nb_var, p_ptr, d_ptr, window)
        dist_obj.additive_dist = d_ptr[0]
        dist_obj.average_dist_1 = d_ptr[1]
        dist_obj.average_dist_2 = d_ptr[2]
        if dist_obj.additive_dist < precision or dist_obj.average_dist_1 < precision \
                or dist_obj.average_dist_2 < precision:
            raise RuntimeError()
        if get_path:
            path_size = int(p_ptr[0])
            all_path = [(int(p_ptr[k]), int(p_ptr[k + 1]), p_ptr[k + 2]) for k in range(1, path_size, 3)]
            all_path.reverse()
            all_path = np.array(all_path, dtype=np.float64)
            dist_obj.path = all_path
    else:
        w_ptr = __ffi.cast("double*", weight.ctypes.data)
        if path is not None:
            indexes = path[:, 0]
            temporal_differences = path[:, 1]
            path_length = len(indexes)
            p_ptr = __ffi.new("int[" + str(path_length) + "]")
            for ind, v in enumerate(indexes):
                p_ptr[ind] = int(v)
            t_ptr = __ffi.cast("double*", temporal_differences.ctypes.data)
            distances = np.zeros(2, dtype=np.float64)
            d_ptr = __ffi.cast("double*", distances.ctypes.data)
            __dllib.dtw_given_path(t_ptr, p_ptr, w_ptr, path_length, d_ptr)
            dist_obj.additive_dist = d_ptr[0]
            dist_obj.average_dist_1 = d_ptr[1]
            if dist_obj.additive_dist < precision or dist_obj.average_dist_1 < precision:
                raise RuntimeError()
        else:
            distances = np.zeros(2, dtype=np.float64)
            d_ptr = __ffi.cast("double*", distances.ctypes.data)
            __dllib.dtw_weighted(a_ptr, b_ptr, len_a, len_b, nb_var, d_ptr, w_ptr, window)
            dist_obj.additive_dist = d_ptr[0]
            dist_obj.average_dist_1 = d_ptr[1]
            if dist_obj.additive_dist < precision or dist_obj.average_dist_1 < precision:
                raise RuntimeError()
    return dist_obj


def cort(series_a, series_b, weight=None, get_path=False, interval=None, path=None, window=None):
    """
    Computes the tdw cort distance between series_a and series_b.

    :param series_a: the first series
    :param series_b: the second series
    :param weight: weight vector to use (related to series_a)
    :param get_path: return the path or not
    :param path: Given path to be used in the distance computation
    :param interval: time interval considered for series_a (full interval if None). Default=None.
    :type interval: a tuple that represents the interval (start included, end excluded) \
                    in the scale of [0, 100] interval are re-normalized for each series.
    :param window: window look-ahead for DTW, CorT and CorT_DTW
    :return: the distance and optional path
    :rtype: float
    """

    nb_var = len(series_a.shape)
    if nb_var > 1:
        series_a = series_a.T
        series_b = series_b.T

    len_a = len(series_a)
    len_b = len(series_b)

    time_start_a = 0 if interval is None else int(interval[0]*len_a/100)
    time_end_a = len_a if interval is None else int(interval[1]*len_a/100)
    time_start_b = 0 if interval is None else int(interval[0]*len_b/100)
    time_end_b = len_b if interval is None else int(interval[1]*len_b/100)

    if time_start_a == time_end_a or time_start_b == time_end_b:
        return sys.float_info.max
    assert time_start_a >= 0,\
        "time_start_a should be positive (actual value={})".format(time_start_a)
    assert time_start_b >= 0, \
        "time_start_b should be positive (actual value={})".format(time_start_b)
    assert time_end_a <= len_a, \
        "time_end_a should not be larger than len(serie_a) {} vs {}, {})".format(time_end_a, len_a, series_a)
    assert time_end_b <= len_b, \
        "time_end_b should not be larger than len(serie_b) {} vs {})".format(time_end_b, len_b)

    a_ptr = __ffi.cast("double*", series_a.ctypes.data)
    b_ptr = __ffi.cast("double*", series_b.ctypes.data)
    dist_obj = Do.DistanceObject()
    temp_window = len(weight) if series_a is None else len(series_a)
    if window is None:
        window = temp_window - 2
    else:
        if 0 < window < 1:
            window = round(temp_window * window) - 2

    if series_a is not None:
        if window < abs(len(series_a) - len(series_b)):
            raise AttributeError('window size is too small to calculate CorT!')

    if window < 0:
        window = 0

    all_path = np.zeros((len_a + len_b) * 3, dtype=np.float64)
    p_ptr = __ffi.cast("double*", all_path.ctypes.data)
    distances = np.zeros(3, dtype=np.float64)
    d_ptr = __ffi.cast("double*", distances.ctypes.data)
    __dllib.cort_window_path(a_ptr, b_ptr, 0, 0, len_a, len_b, nb_var, 0, p_ptr, d_ptr, window)
    dist_obj.additive_dist = d_ptr[0]
    dist_obj.average_dist_1 = d_ptr[1]
    dist_obj.average_dist_2 = d_ptr[2]
    if dist_obj.additive_dist < precision or dist_obj.average_dist_1 < precision \
            or dist_obj.average_dist_2 < precision:
        raise RuntimeError()
    if get_path:
        path_size = int(p_ptr[0])
        all_path = [(int(p_ptr[k]), int(p_ptr[k + 1]), p_ptr[k + 2]) for k in range(1, path_size, 3)]
        all_path.reverse()
        all_path = np.array(all_path, dtype=np.float64)
        dist_obj.path = all_path
    return dist_obj


def cort_dtw(series_a, series_b, weight=None, get_path=False, interval=None, path=None, window=None):
    """
    Computes the cort_dtw distance between series_a and series_b.

    :param series_a: the first series
    :param series_b: the second series
    :param weight: weight vector to use (related to series_a)
    :param get_path: return the path or not
    :param path: Given path to be used in the distance computation
    :param interval: time interval considered for serie_a (full interval if None). Default=None.
    :type interval: a tuple that represents the interval (start included, end excluded) \
                    in the scale of [0, 100] interval are re-normalized for each series.
    :param window: window look-ahead for DTW, CorT and CorT_DTW
    :return: the distance and optional path
    :rtype: float
    """

    nb_var = len(series_a.shape)
    if nb_var > 1:
        series_a = series_a.T
        series_b = series_b.T

    len_a = len(series_a)
    len_b = len(series_b)

    time_start_a = 0 if interval is None else int(interval[0]*len_a/100)
    time_end_a = len_a if interval is None else int(interval[1]*len_a/100)
    time_start_b = 0 if interval is None else int(interval[0]*len_b/100)
    time_end_b = len_b if interval is None else int(interval[1]*len_b/100)

    if time_start_a == time_end_a or time_start_b == time_end_b:
        return sys.float_info.max
    assert time_start_a >= 0,\
        "time_start_a should be positive (actual value={})".format(time_start_a)
    assert time_start_b >= 0, \
        "time_start_b should be positive (actual value={})".format(time_start_b)
    assert time_end_a <= len_a, \
        "time_end_a should not be larger than len(serie_a) {} vs {}, {})".format(time_end_a, len_a, series_a)
    assert time_end_b <= len_b, \
        "time_end_b should not be larger than len(serie_b) {} vs {})".format(time_end_b, len_b)

    a_ptr = __ffi.cast("double*", series_a.ctypes.data)
    b_ptr = __ffi.cast("double*", series_b.ctypes.data)
    dist_obj = Do.DistanceObject()
    temp_window = len(weight) if series_a is None else len(series_a)
    if window is None:
        window = temp_window - 2
    else:
        if 0 < window < 1:
            window = round(temp_window * window) - 2

    if series_a is not None:
        if window < abs(len(series_a) - len(series_b)):
            raise AttributeError('window size is too small to calculate CorT_DTW!')

    if window < 0:
        window = 0

    all_path = np.zeros((len_a + len_b) * 3, dtype=np.float64)
    p_ptr = __ffi.cast("double*", all_path.ctypes.data)
    distances = np.zeros(3, dtype=np.float64)
    d_ptr = __ffi.cast("double*", distances.ctypes.data)
    __dllib.cort_dtw_window_path(a_ptr, b_ptr, 0, 0, len_a, len_b, nb_var, 0, p_ptr, d_ptr, window)
    dist_obj.additive_dist = d_ptr[0]
    dist_obj.average_dist_1 = d_ptr[1]
    dist_obj.average_dist_2 = d_ptr[2]
    if dist_obj.additive_dist < precision or dist_obj.average_dist_1 < precision \
            or dist_obj.average_dist_2 < precision:
        raise RuntimeError()
    if get_path:
        path_size = int(p_ptr[0])
        all_path = [(int(p_ptr[k]), int(p_ptr[k + 1]), p_ptr[k + 2]) for k in range(1, path_size, 3)]
        all_path.reverse()
        all_path = np.array(all_path, dtype=np.float64)
        dist_obj.path = all_path
    return dist_obj
