import numpy as np
import tdt.decisiontree.core.LearningSet as Ls
import tdt.decisiontree.core.Observation as Obs
import tdt.decisiontree.core.Timeseries as Ts
import tdt.decisiontree.core.Type as Tp
import tdt.decisiontree.core.Variable as Vrb
import tdt.decisiontree.core.Utility as Util
from collections import OrderedDict
from ikatsbackend.core.resource.api import IkatsApi as Api

# Import logger
import logging

# Logger definition
LOGGER = logging.getLogger(__name__)


class LearningSetBuilder(object):

    """
    A builder for the learningset, this class will handle different format of dataset
    (csv, one line per TS, one file per TS, ...)
    """

    def __init__(self):

        self._build_learningset = {'ikats': self.build_from_ikats, 'ikats_test': self.build_from_ikats_test,
                                   'ikats_predict': self.build_from_ikats_predict}

    def build(self, ls_format,
              input_file=None,
              delimiter=',',
              ls_name='learningset',
              ts_prefix='TS_',
              obs_prefix='obs_',
              rhm_patterns=None):

        return self._build_learningset[ls_format](input_file, delimiter, ls_name, ts_prefix, obs_prefix, rhm_patterns)

    def build_from_ikats(self, input_file, delimiter, ls_name, ts_prefix, obs_prefix, rhm_patterns):

        """
        Extract information from a table and format the output as a dict of dict
        The first key will be the obs_id values taken from the table_content.

        :param ls_table: the JSON content corresponding to the table as dict
        :param id_label: Column name used as primary key
        :param id_class: Column name used as target key

        :type ls_table: dict
        :type id_label: str
        :type id_class: str

        :return: a dict of dict where first key is the obs_id and the sub keys are the items
        :rtype: dict
        """

        # 2D array containing the equivalent of the rendered JSON structure
        data_array = []
        variables = []
        columns_name = dict()

        ls_table = Api.table.read(ls_name)
        ls_variables = []
        observations = []
        all_data = []

        rhm_variables = []

        # Pattern (RHM) part
        if rhm_patterns is not None:
            patterns = rhm_patterns['patterns']
            break_points = rhm_patterns['break_points']
            disc_points = rhm_patterns['disc_break_points']

            for pat in sorted(patterns):
                for indicator in ['count', 'exist']:
                    rhm_variables.append(self.get_info(patterns[pat], indicator, break_points, disc_points))

            for ind in range(len(ls_table["content"]["cells"])):
                data = []
                for pat_key in sorted(patterns):
                    pat_obj = patterns[pat_key]
                    locations = pat_obj['locations']
                    ts_name = 'TS' + str(ind)
                    if ts_name in locations:
                        data.append(len(locations[ts_name]))
                        data.append(1)
                    else:
                        data.append(0)
                        data.append(0)
                all_data.append(data)

        # End of Pattern part

        try:
            # Get the columns name with a mapping dict
            for v, k in enumerate(ls_table["headers"]["col"]["data"]):
                columns_name[k] = v
                variables.append(k)
                ls_variables.append(Vrb.Variable(k, 'float', Tp.Type.TIMESERIES, (-5, 5)))

        except:
            raise ValueError("Table content shall contain col headers to know the name of columns")

        """
        if id_label not in columns_name or id_class not in columns_name:
            raise ValueError("Table header should contain a unique ID and a target column")
        """

        try:
            # Fill the 2D array with the content of the header column
            # Skip the first cell by starting at index 1
            data_array = list(map(lambda x: [x], ls_table["headers"]["row"]["data"][1:]))
        except KeyError:
            # No header column present, skip it
            pass

        items = variables[1:len(variables) - 1]
        id_class = len(variables) - 1

        for line_index, line in enumerate(ls_table["content"]["cells"]):

            if len(data_array) < line_index:
                # Fill in the data_array line with an empty list in case there was no header column
                data_array.append([])
            data_array[line_index].extend(line)
            null_check = False
            if rhm_patterns is not None:
                data = all_data[line_index]
            else:
                data = []
            obs_id = obs_prefix + str(line_index).zfill(3)
            for item in items:
                item_ind = columns_name[item]
                func_id = data_array[line_index][item_ind]
                tsuid = None
                for link in ls_table['content']['links'][line_index]:
                    if link:
                        if func_id == link['val'][0]['funcId']:
                            tsuid = link['val'][0]['tsuid']
                            break
                        else:
                            continue

                if tsuid is None:
                    null_check = True
                    break
                temp_ts_data = Api.ts.read(tsuid_list=[tsuid])[0]
                ts_name = ts_prefix + str(line_index).zfill(3) + '_' + item
                data.append(Ts.Timeseries(name=func_id, tsuid=tsuid,
                                          data=np.array(temp_ts_data[:, 1], dtype=np.float64)))
            if null_check:
                continue
            data = np.array(data, dtype=object)
            observation = Obs.Observation(obs_id=obs_id, data=data, label=str(data_array[line_index][id_class]))
            observations.append(observation)

        ls_variables = rhm_variables + ls_variables[1: len(ls_variables) - 1]
        return Ls.LearningSet(name=ls_name,
                              observations=np.array(observations, dtype=object),
                              variables=np.array(ls_variables, dtype=object))

    def build_from_ikats_test(self, input_file, delimiter, ls_name, ts_prefix, obs_prefix, rhm_patterns):

        """
        Extract information from a table and format the output as a dict of dict
        The first key will be the obs_id values taken from the table_content.

        :param ls_table: the JSON content corresponding to the table as dict
        :param id_label: Column name used as primary key
        :param id_class: Column name used as target key

        :type ls_table: dict
        :type id_label: str
        :type id_class: str

        :return: a dict of dict where first key is the obs_id and the sub keys are the items
        :rtype: dict
        """

        # 2D array containing the equivalent of the rendered JSON structure
        data_array = []
        variables = []
        columns_name = dict()

        ls_table = Api.table.read(ls_name)
        ls_variables = []
        observations = []

        rhm_variables = []
        break_points = None
        # Pattern (RHM) part
        if rhm_patterns is not None:
            patterns = rhm_patterns['patterns']
            break_points = rhm_patterns['break_points']
            disc_points = rhm_patterns['disc_break_points']

            for pat in sorted(patterns):
                for indicator in ['count', 'exist']:
                    rhm_variables.append(self.get_info(patterns[pat], indicator, break_points, disc_points))

        # End of Pattern part

        try:
            # Get the columns name with a mapping dict
            for v, k in enumerate(ls_table["headers"]["col"]["data"]):
                columns_name[k] = v
                variables.append(k)
                ls_variables.append(Vrb.Variable(k, 'float', Tp.Type.TIMESERIES, (-5, 5)))

        except:
            raise ValueError("Table content shall contain col headers to know the name of columns")

        """
        if id_label not in columns_name or id_class not in columns_name:
            raise ValueError("Table header should contain a unique ID and a target column")
        """

        try:
            # Fill the 2D array with the content of the header column
            # Skip the first cell by starting at index 1
            data_array = list(map(lambda x: [x], ls_table["headers"]["row"]["data"][1:]))
        except KeyError:
            # No header column present, skip it
            pass

        items = variables[1:len(variables) - 1]
        id_class = len(variables) - 1

        for line_index, line in enumerate(ls_table["content"]["cells"]):

            if len(data_array) < line_index:
                # Fill in the data_array line with an empty list in case there was no header column
                data_array.append([])
            data_array[line_index].extend(line)
            null_check = False
            data = []
            obs_id = obs_prefix + str(line_index).zfill(3)
            for item in items:
                item_ind = columns_name[item]
                func_id = data_array[line_index][item_ind]
                tsuid = None
                for link in ls_table['content']['links'][line_index]:
                    if link:
                        if func_id == link['val'][0]['funcId']:
                            tsuid = link['val'][0]['tsuid']
                            break
                        else:
                            continue

                if tsuid is None:
                    null_check = True
                    break
                temp_ts_data = Api.ts.read(tsuid_list=[tsuid])[0]

                if rhm_patterns is not None:
                    digit_data = Util.discretize_ts(np.array(temp_ts_data[:, 1], dtype=np.float64), break_points)
                    LOGGER.debug(str(digit_data))
                    for pat in sorted(patterns):
                        pattern = patterns[pat]
                        pat_len = pattern['length']
                        _reg_pat = Util.pattern_to_re3(pattern['regex'])
                        positions = Util.pattern_matcher(_reg_pat, pat_len, digit_data)
                        count = len(positions)
                        exist = 1 if count > 0 else 0
                        data.extend([count, exist])

                ts_name = ts_prefix + str(line_index).zfill(3) + '_' + item
                data.append(Ts.Timeseries(name=func_id, tsuid=tsuid,
                                          data=np.array(temp_ts_data[:, 1], dtype=np.float64)))
            if null_check:
                continue
            data = np.array(data, dtype=object)
            observation = Obs.Observation(obs_id=obs_id, data=data, label=str(data_array[line_index][id_class]))
            observations.append(observation)

        ls_variables = rhm_variables + ls_variables[1: len(ls_variables) - 1]
        return Ls.LearningSet(name=ls_name,
                              observations=np.array(observations, dtype=object),
                              variables=np.array(ls_variables, dtype=object))

    def build_from_ikats_predict(self, input_file, delimiter, ls_name, ts_prefix, obs_prefix, rhm_patterns):

        """
        Extract information from a table and format the output as a dict of dict
        The first key will be the obs_id values taken from the table_content.

        :param ls_table: the JSON content corresponding to the table as dict
        :param id_label: Column name used as primary key
        :param id_class: Column name used as target key

        :type ls_table: dict
        :type id_label: str
        :type id_class: str

        :return: a dict of dict where first key is the obs_id and the sub keys are the items
        :rtype: dict
        """

        # 2D array containing the equivalent of the rendered JSON structure
        data_array = []
        variables = []
        columns_name = dict()

        ls_table = Api.table.read(ls_name)
        ls_variables = []
        observations = []

        rhm_variables = []
        break_points = None
        # Pattern (RHM) part
        if rhm_patterns is not None:
            patterns = rhm_patterns['patterns']
            break_points = rhm_patterns['break_points']
            disc_points = rhm_patterns['disc_break_points']

            for pat in sorted(patterns):
                for indicator in ['count', 'exist']:
                    rhm_variables.append(self.get_info(patterns[pat], indicator, break_points, disc_points))

        # End of Pattern part

        try:
            # Get the columns name with a mapping dict
            for v, k in enumerate(ls_table["headers"]["col"]["data"]):
                columns_name[k] = v
                variables.append(k)
                ls_variables.append(Vrb.Variable(k, 'float', Tp.Type.TIMESERIES, (-5, 5)))

        except:
            raise ValueError("Table content shall contain col headers to know the name of columns")

        """
        if id_label not in columns_name or id_class not in columns_name:
            raise ValueError("Table header should contain a unique ID and a target column")
        """

        try:
            # Fill the 2D array with the content of the header column
            # Skip the first cell by starting at index 1
            data_array = list(map(lambda x: [x], ls_table["headers"]["row"]["data"][1:]))
        except KeyError:
            # No header column present, skip it
            pass

        items = variables[1:len(variables) - 1]

        for line_index, line in enumerate(ls_table["content"]["cells"]):

            if len(data_array) < line_index:
                # Fill in the data_array line with an empty list in case there was no header column
                data_array.append([])
            data_array[line_index].extend(line)
            null_check = False
            data = []
            obs_id = obs_prefix + str(line_index).zfill(3)
            for item in items:
                item_ind = columns_name[item]
                func_id = data_array[line_index][item_ind]
                tsuid = None
                for link in ls_table['content']['links'][line_index]:
                    if link:
                        if func_id == link['val'][0]['funcId']:
                            tsuid = link['val'][0]['tsuid']
                            break
                        else:
                            continue

                if tsuid is None:
                    null_check = True
                    break
                temp_ts_data = Api.ts.read(tsuid_list=[tsuid])[0]

                if rhm_patterns is not None:
                    digit_data = Util.discretize_ts(np.array(temp_ts_data[:, 1], dtype=np.float64), break_points)
                    LOGGER.debug(str(digit_data))
                    for pat in sorted(patterns):
                        pattern = patterns[pat]
                        pat_len = pattern['length']
                        _reg_pat = Util.pattern_to_re3(pattern['regex'])
                        positions = Util.pattern_matcher(_reg_pat, pat_len, digit_data)
                        count = len(positions)
                        exist = 1 if count > 0 else 0
                        data.extend([count, exist])

                ts_name = ts_prefix + str(line_index).zfill(3) + '_' + item
                data.append(Ts.Timeseries(name=func_id, tsuid=tsuid,
                                          data=np.array(temp_ts_data[:, 1], dtype=np.float64)))
            if null_check:
                continue
            data = np.array(data, dtype=object)
            observation = Obs.Observation(obs_id=obs_id, data=data)
            observations.append(observation)

        ls_variables = rhm_variables + ls_variables[1: len(ls_variables) - 1]
        return Ls.LearningSet(name=ls_name,
                              observations=np.array(observations, dtype=object),
                              variables=np.array(ls_variables, dtype=object))

    def build_from_learningset(self, orig_learningset, keys):

        new_observations = orig_learningset.observations[keys]
        return Ls.LearningSet(name=orig_learningset.name, observations=new_observations,
                              variables=orig_learningset.variables, meta_data=orig_learningset.meta_data,
                              timestamp=orig_learningset.timestamp)

    def get_info(self, pattern, indicator, break_points, disc_points):

        variable = Vrb.Variable(v_type='float', imp_type=Tp.Type.PATTERN)
        variable.name = pattern['regex'] + '(' + pattern['variable'] + ')/' + indicator
        p_description = OrderedDict()
        p_description['generator'] = ['RHM', ['version', 1.0]]

        # for now, need to be modified
        p_vars = [{'name': v, 'breakpoints': break_points[ind], 'domain': disc_points[ind]}
                  for ind, v in enumerate(pattern['variable'].split('-'))]

        p_description['definition'] = pattern['regex']
        p_description['slice'] = [0, 100]  # for now
        p_description['operator'] = indicator
        p_description['variables'] = p_vars
        variable.description = p_description
        return variable
