
class WeightObject(object):

    def __init__(self, data=None, unnormalized_sum=0):

        self._data = data
        self._unnormalized_sum = round(unnormalized_sum, 4)

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value

    @property
    def unnormalized_sum(self):
        return self._unnormalized_sum

    @unnormalized_sum.setter
    def unnormalized_sum(self, value):
        self._unnormalized_sum = value
