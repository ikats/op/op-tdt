
class Timeseries(object):

    """
    A class representation of a timeseries, a timeseries has a name, a data (numpy array or matrix)
    and a length (different timeseries has different length)
    """

    def __init__(self, name=None, data=None, tsuid=None):

        """

        :param name: name of the timeseries (string)
        :param data: a numpy array or matrix
        """

        self._name = name
        self._data = data
        self._tsuid = tsuid

        if data is not None:
            self._length = len(data)
        else:
            self._length = None

    @property
    def name(self):
        if len(self.data.shape) == 1:
            return [self._name]
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def tsuid(self):
        return self._tsuid

    @tsuid.setter
    def tsuid(self, value):
        self._tsuid = value

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value
        self._length = len(self._data)

    @property
    def length(self):
        return self._length

    @length.setter
    def length(self, value):
        self._length = value
