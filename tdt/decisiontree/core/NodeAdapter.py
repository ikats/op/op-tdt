class NodeAdapter(object):

    """
    An adapter for the TreeNode
    """

    def __init__(self,
                 gain=None,
                 children=None,
                 data=None):

        self._gain = gain
        self._children = children
        if data:
            self._data = data
        else:
            self._data = dict()

    @property
    def gain(self):
        return self._gain

    @gain.setter
    def gain(self, value):
        self._gain = value

    @property
    def children(self):
        return self._children

    @children.setter
    def children(self, value):
        self._children = value

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value
