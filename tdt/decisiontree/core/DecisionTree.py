import collections
import json
import sys
import copy
import numpy as np
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
import tdt.decisiontree.core.Utility as Util
import tdt.decisiontree.core.PruningObject as Po


class DecisionTree(object):

    """
    The DecisionTree class contains the final representation of the Tree object,
    also, the object will have a json structure for the ease of use
    in term of readability and prediction.
    """

    def __init__(self,
                 header=None,
                 tree=None,
                 validation=None,
                 data=None,
                 arguments=None,
                 depth=0):

        """

        :param header: contains information about the learningset
        :param tree: an array of nodes created by the builder (Should be converted easily to TreeNode)
        :param validation: contains information about the test experiments
        :param data: an array timeseries raw data with their useful information (class, weights)
        :param arguments: arguments/parameters used to build the tree.
        :param depth: depth of the tree
        """

        if header is None:
            self._header = dict()
        else:
            self._header = header

        if tree is None:
            self._tree = []
        else:
            self._tree = tree

        if data is None:
            self._data = []
        else:
            self._data = data

        if validation is None:
            self._validation = dict()
            self._validation['arguments'] = arguments
        else:
            self._validation = validation

        self._depth = depth

    @property
    def header(self):
        return self._header

    @header.setter
    def header(self, value):
        self._header = value

    @property
    def tree(self):
        return self._tree

    @tree.setter
    def tree(self, value):
        self._tree = value

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value

    @property
    def validation(self):
        return self._validation

    @validation.setter
    def validation(self, value):
        self._validation = value

    @property
    def depth(self):
        return self._depth

    @depth.setter
    def depth(self, value):
        self._depth = value

    @property
    def nb_nodes(self):
        return len(self._tree)

    def save(self, output='tree'):

        """
        Save the tree to a json file
        :param output: Path to the output file
        """

        tree = collections.OrderedDict()
        tree['header'] = self._header
        tree['tree'] = self._tree
        tree['validation'] = self._validation
        tree['data'] = self._data
        if output:

            with open(output, 'w') as outfile:
                json.dump(tree, outfile)

    def copy(self, tree_path):

        """
        Parse a tree file (json format
        :param tree_path: Json tree path
        :return: Copy the tree into the current object
        """
        with open(tree_path, 'r') as json_tree:
            content = json_tree.read()
        tree = json.loads(content)
        self.data = tree['data']
        self.validation = tree['validation']
        self.header = tree['header']
        self.tree = tree['tree']

    def parse(self, tree):

        """
        Parse a tree file (json format
        :param tree_path: Json tree path
        :return: Copy the tree into the current object
        """
        # tree = json.loads(tree_json)
        self.data = tree['data']
        self.validation = tree['validation']
        self.header = tree['header']
        self.tree = tree['tree']

    def predict(self, new_set, method=None):

        """
        :param new_set: This set needs to have the LearningSet structure
        :param method: Testing method dictionary
        :return: an array of tuple (predicted_label, true_label)
        """
        traces = []

        # The VariableName VariableIndex mapper
        var_ind_mapper = new_set.var_ind_mapper
        predictions = []
        golds = []
        labels = sorted(new_set.labels_set)
        labels_distribution = new_set.count_lb_stats()
        labels_prediction = {l: 0 for l in labels}
        label_per = 1 / len(labels)

        # unbalanced_tree = list(filter(lambda s: s[0] == 'unbalanced-tree', self.validation['arguments']))[0][1]
        # if unbalanced_tree:
        #     stats_weights = {c[0]: 1/c[1] for c in stats_weights}
        # else:
        #     stats_weights = {c[0]: 1 for c in stats_weights}

        for observation in new_set.observations:
            golds.append(observation.label)
            obs_trace = []
            current_node = self._tree[0]
            # While the node is not a leaf
            while current_node['type'] != 'leaf':
                obs_trace.append(current_node['node'])

                # Get the variable data
                description = current_node['description']
                var_index = description['variable_id']
                if current_node['type'] in ['comparison', 'similarity']:
                    obs_data = observation[var_index].data
                else:
                    obs_data = observation[var_index]

                # Comparison/HyperPlane Node
                if current_node['type'] == 'comparison':
                    current_node = self.compare_comparison(obs_data, current_node)

                # Similarity/HyperSphere Node
                elif current_node['type'] == 'similarity':
                    current_node = self.compare_similarity(obs_data, current_node)

                # Move toward the satisfied condition
                else:
                    current_node = self.compare_variable(obs_data, current_node)
            else:
                obs_trace.append(current_node['node'])
                majority_label_per = 0
                majority_label = None
                for stats in current_node['statistics']:  # e.g. statistics = [['c1', 25], ['c2', 2], ...]
                    # temp_major_per = stats[1] * stats_weights[stats[0]]
                    temp_major_per = stats[1]
                    if temp_major_per > majority_label_per:
                        majority_label_per = temp_major_per
                        majority_label = stats[0]
                predictions.append(majority_label)
                if majority_label == observation.label:
                    labels_prediction[majority_label] += 1
            traces.append([observation.obs_id, observation.label] + [obs_trace])

        labels_prediction = [(labels_prediction[l]/labels_distribution[l]) * label_per for l in labels]
        accuracy = accuracy_score(golds, predictions)
        conf_matrix = confusion_matrix(golds, predictions, labels=labels).T
        conf_matrix = [[int(v) for v in arr] for arr in conf_matrix]
        conf_matrix = [labels] + conf_matrix
        if method is None:
            self._validation['method'] = ['Test-Set', new_set.name]
        else:
            self._validation['method'] = method
        self._validation['accuracy'] = [round(accuracy, 4)]
        self._validation['weighted-accuracy'] = [round(sum(labels_prediction), 4)]
        self._validation['confusion-matrix'] = conf_matrix
        self._validation['trace'] = traces

    def true_predict(self, new_set, method=None):

        """
        :param new_set: This set needs to have the LearningSet structure
        :param method: Testing method dictionary
        :return: an array of tuple (predicted_label, true_label)
        """
        traces = []

        # The VariableName VariableIndex mapper
        var_ind_mapper = new_set.var_ind_mapper
        predictions = []
        golds = []

        # unbalanced_tree = list(filter(lambda s: s[0] == 'unbalanced-tree', self.validation['arguments']))[0][1]
        # if unbalanced_tree:
        #     stats_weights = {c[0]: 1/c[1] for c in stats_weights}
        # else:
        #     stats_weights = {c[0]: 1 for c in stats_weights}

        for observation in new_set.observations:
            golds.append(observation.label)
            obs_trace = []
            current_node = self._tree[0]
            # While the node is not a leaf
            while current_node['type'] != 'leaf':
                obs_trace.append(current_node['node'])

                # Get the variable data
                description = current_node['description']
                var_index = description['variable_id']
                if current_node['type'] in ['comparison', 'similarity']:
                    obs_data = observation[var_index].data
                else:
                    obs_data = observation[var_index]

                # Comparison/HyperPlane Node
                if current_node['type'] == 'comparison':
                    current_node = self.compare_comparison(obs_data, current_node)

                # Similarity/HyperSphere Node
                elif current_node['type'] == 'similarity':
                    current_node = self.compare_similarity(obs_data, current_node)

                # Move toward the satisfied condition
                else:
                    current_node = self.compare_variable(obs_data, current_node)
            else:
                obs_trace.append(current_node['node'])
                majority_label_per = 0
                majority_label = None
                for stats in current_node['statistics']:  # e.g. statistics = [['c1', 25], ['c2', 2], ...]
                    # temp_major_per = stats[1] * stats_weights[stats[0]]
                    temp_major_per = stats[1]
                    if temp_major_per > majority_label_per:
                        majority_label_per = temp_major_per
                        majority_label = stats[0]
                predictions.append(majority_label)
            traces.append([observation.obs_id, '?'] + [obs_trace])

        if method is None:
            self._validation['method'] = ['Test-Set', new_set.name]
        else:
            self._validation['method'] = method

        return predictions

    def compare_comparison(self, new_obs_data, current_node):

        """
        Predict the next node to move if the current node is a comparison node (HyperPlane)
        :param new_obs_data: an np array (timeseries)
        :param current_node: current node
        :return: The index of the next node.
        """

        use_path = list(filter(lambda s: s[0] == 'store-path', self.validation['arguments']))[0][1]
        window = list(filter(lambda s: s[0] == 'window', self.validation['arguments']))[0][1]
        description = current_node['description']
        distance_msr = description['distance'][0]
        weight_type = description['weight'][0]
        method = description['distance'][1][1]
        max_val = sys.float_info.max
        child = None

        # Choose the best similar timeseries
        for crt in description['criteria']:
            ts_data = []
            weights = None
            for ts_name in crt['sequence']:
                data_obj = list(filter(lambda s: s['sequence'] == ts_name, self._data))[0]
                ts_data.append(np.array(data_obj['value'], dtype=np.float64))
                weights = list(filter(lambda s: s[0] == current_node['node'], data_obj['weight']))[0]
            weights = np.array(weights[1], dtype=np.float64)
            if len(crt['sequence']) > 1:
                ts_data = np.array(ts_data)
            else:
                ts_data = ts_data[0]

            if use_path and distance_msr == 'dtw' and weight_type != 'uniform':
                dist_obj = Util.get_distance(x=ts_data, y=new_obs_data, measure=distance_msr,
                                             get_path=True, window=window)
                dist_obj = Util.get_distance(path=dist_obj.path, w=weights, measure=distance_msr, window=window)
            else:
                dist_obj = Util.get_distance(x=ts_data, y=new_obs_data, w=weights, measure=distance_msr, window=window)
            if method == 'add':
                dist_val = dist_obj.additive_dist
            else:
                dist_val = dist_obj.average_dist_1

            if dist_val < max_val:
                max_val = dist_val
                child = crt['child']
        return list(filter(lambda s: s['node'] == child, self._tree))[0]

    def compare_similarity(self, new_obs_data, current_node):

        """
        Predict the next node to move if the current node is a similarity node (HyperSphere)
        :param new_obs_data: an np array (timeseries)
        :param current_node: current node
        :return: The index of the next node.
        """
        use_path = list(filter(lambda s: s[0] == 'store-path', self.validation['arguments']))[0][1]
        window = list(filter(lambda s: s[0] == 'window', self.validation['arguments']))[0][1]
        description = current_node['description']
        criteria = description['criteria']
        weight_type = description['weight'][0]
        distance_msr = description['distance'][0]
        method = description['distance'][1][1]
        threshold_value = criteria[0]['value']
        ts_data = []
        weights = None
        for seq_id in description['sequence']:
            data_obj = list(filter(lambda s: s['sequence'] == seq_id, self._data))[0]
            ts_data.append(np.array(data_obj['value'], dtype=np.float64))
            weights = list(filter(lambda s: s[0] == current_node['node'], data_obj['weight']))[0]
            weights = np.array(weights[1], dtype=np.float64)
        if len(description['sequence']) > 1:
            ts_data = np.array(ts_data)
        else:
            ts_data = ts_data[0]

        if use_path and distance_msr == 'dtw' and weight_type != 'uniform':
            dist_obj = Util.get_distance(x=ts_data, y=new_obs_data, measure=distance_msr, get_path=True, window=window)
            dist_obj = Util.get_distance(path=dist_obj.path, w=weights, measure=distance_msr, window=window)
        else:
            dist_obj = Util.get_distance(x=ts_data, y=new_obs_data, w=weights, measure=distance_msr, window=window)

        if method == 'add':
            dist_val = dist_obj.additive_dist
        else:
            dist_val = dist_obj.average_dist_1

        if dist_val < threshold_value:
            return list(filter(lambda s: s['node'] == criteria[0]['child'], self._tree))[0]
        else:
            return list(filter(lambda s: s['node'] == criteria[1]['child'], self._tree))[0]

    def compare_variable(self, new_obs_data, current_node):

        """
        Predict the next node to move if the current node is a standard node (nominal, continuous or a pattern)
        :param new_obs_data: a value (string, float, ...)
        :param current_node: current node
        :return: The index of the next node.
        """

        description = current_node['description']
        for seq in description['criteria']:
            test = seq['test']
            value = seq['value']
            condition = Util.check_condition(new_obs_data, value, test)
            if condition:
                return list(filter(lambda s: s['node'] == seq['child'], self._tree))[0]

    def post_prune(self, validation_set, learning_accuracy=0):

        """
        Post-prune the tree based on the accuracy obtained on validation_set

        :param validation_set: validation set
        :param learning_accuracy: base accuracy rate for the pruning
        :return: A set of trees with their respective accuracy rate
        """

        parent_children_list = []
        for node in self._tree:

            if node['type'] == 'leaf':
                continue

            children = []
            description = node['description']
            for child in description['criteria']:
                children.append(child['child'])

            parent_children_list.append(Po.PruningObject(parent_id=node['node'],
                                                         children_list=children,
                                                         depth=node['depth']))

        parent_children_list.sort(reverse=True)
        roll_bak = dict()
        to_remove = []
        to_roll_back = []

        pruned_tree = copy.deepcopy(self)
        for branch in parent_children_list:
            parent_id = 0
            parent_node = None
            for ind, node in enumerate(pruned_tree.tree):
                if node['node'] == branch.parent_id:
                    parent_id = ind
                    parent_node = node
                    break
            roll_bak[parent_id] = copy.deepcopy(parent_node)

            statistics = parent_node['statistics']
            maj_label = 0
            leaf_label = None
            for stats in statistics:
                if stats[1] > maj_label:
                    maj_label = stats[1]        # label count
                    leaf_label = stats[0]       # label

            leaf_type = 'leaf'
            new_leaf_name = leaf_type + ': majority class ' + str(leaf_label)
            parent_node['type'] = leaf_type
            parent_node['name'] = new_leaf_name
            parent_node['label'] = leaf_label
            observations = []

            for ind, child_node in enumerate(pruned_tree.tree):
                if child_node['node'] in branch.children_list:
                    observations.extend(child_node['observations'])
            parent_node['observations'] = observations

            test_res = pruned_tree.predict(validation_set)
            acc = 0
            for res in test_res:
                if res[0] == res[1]:
                    acc += 1

            test_acc = float(acc) / float(len(test_res))

            if test_acc >= learning_accuracy:
                del parent_node['description']
                learning_accuracy = test_acc
                to_remove.extend(branch.children_list)
            else:
                to_roll_back.append(parent_id)

        for node_ind in to_roll_back:
            pruned_tree.tree[node_ind] = roll_bak[node_ind]

        pruned_tree.tree = [node for node in pruned_tree.tree if node['node'] not in to_remove]

        # We still need information about the best tree (Accuracy, confusion matrix, ...)
        # TO DO

        return pruned_tree
