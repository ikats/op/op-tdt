import numpy as np


class DistanceMatrix(object):

    def __init__(self,
                 var_id=None,
                 data_length=None,
                 is_dictionary=False,
                 distance_measure='l2',
                 weight_type='uniform',
                 weight_factor=1):

        self._var_id = var_id
        self._is_dictionary = is_dictionary
        self._distance_measure = distance_measure
        self._weight_type = weight_type
        self._weight_factor = weight_factor

        if is_dictionary:
            self._values = dict()
        else:
            self._values = np.zeros((data_length, data_length), dtype=np.float64)

    @property
    def var_id(self):
        return self._var_id

    @var_id.setter
    def var_id(self, value):
        self._var_id = value

    @property
    def is_dictionary(self):
        return self._is_dictionary

    @is_dictionary.setter
    def is_dictionary(self, value):
        self._is_dictionary = value

    @property
    def distance_measure(self):
        return self._distance_measure

    @distance_measure.setter
    def distance_measure(self, value):
        self._distance_measure = value

    @property
    def weight_type(self):
        return self._weight_type

    @weight_type.setter
    def weight_type(self, value):
        self._weight_type = value

    @property
    def weight_factor(self):
        return self._weight_factor

    @weight_factor.setter
    def weight_factor(self, value):
        self._weight_factor = value

    @property
    def values(self):
        return self._values

    @values.setter
    def values(self, value):
        self._values = value
