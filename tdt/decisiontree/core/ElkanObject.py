
class ElkanObject(object):

    def __init__(self,
                 left_key=None,
                 right_key=None,
                 parent=None,
                 nb_dist=0,
                 neighbours=None,
                 children=None):

        self._left_key = left_key
        self._right_key = right_key
        self._parent = parent
        self._gain = 1.0
        self._upper_bounds = dict()
        self._lower_bounds = dict()
        self._nb_dist = nb_dist
        self._neighbours = neighbours
        self._children = children

    @property
    def left_key(self):
        return self._left_key

    @left_key.setter
    def left_key(self, value):
        self._left_key = value

    @property
    def right_key(self):
        return self._right_key

    @right_key.setter
    def right_key(self, value):
        self._right_key = value

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, value):
        self._parent = value

    @property
    def gain(self):
        return self._gain

    @gain.setter
    def gain(self, value):
        self._gain = value

    @property
    def nb_dist(self):
        return self._nb_dist

    @nb_dist.setter
    def nb_dist(self, value):
        self._nb_dist = value

    @property
    def neighbours(self):
        return self._neighbours

    @neighbours.setter
    def neighbours(self, value):
        self._neighbours = value

    @property
    def children(self):
        return self._children

    @children.setter
    def children(self, value):
        self._children = value

    def lower_bounds(self, k):
        if k not in self._lower_bounds:
            self._lower_bounds[k] = dict()
            self._lower_bounds[k][self._right_key] = set()
            self._lower_bounds[k][self._left_key] = set()

            self._lower_bounds[k][self._left_key].add(self._right_key[1])
            self._lower_bounds[k][self._right_key].add(self._left_key[1])
        return self._lower_bounds[k]

    def upper_bounds(self, k):
        if k not in self._upper_bounds:
            self._upper_bounds[k] = dict()
            self._upper_bounds[k][self._right_key] = set()
            self._upper_bounds[k][self._left_key] = set()

            self._upper_bounds[k][self._left_key].add(self._left_key[1])
            self._upper_bounds[k][self._right_key].add(self._right_key[1])
        return self._upper_bounds[k]

    def add_key(self, key, k, ref, is_upper=True):

        if ref is self._left_key:
            other_ref = self._right_key
        else:
            other_ref = self._left_key

        if k not in self._lower_bounds:
            self._lower_bounds[k] = dict()
            self._lower_bounds[k][self._right_key] = set()
            self._lower_bounds[k][self._left_key] = set()

            self._lower_bounds[k][self._left_key].add(self._right_key[1])
            self._lower_bounds[k][self._right_key].add(self._left_key[1])

        if k not in self._upper_bounds:
            self._upper_bounds[k] = dict()
            self._upper_bounds[k][self._right_key] = set()
            self._upper_bounds[k][self._left_key] = set()

            self._upper_bounds[k][self._left_key].add(self._left_key[1])
            self._upper_bounds[k][self._right_key].add(self._right_key[1])

        if is_upper:
            self._upper_bounds[k][ref].add(key)
            if key in self._upper_bounds[k][other_ref]:
                self._upper_bounds[k][other_ref].remove(key)
        else:
            self._lower_bounds[k][ref].add(key)
            if key in self._lower_bounds[k][other_ref]:
                self._lower_bounds[k][other_ref].remove(key)

    @property
    def repr(self):
        return self._left_key[1], self._right_key[1]

    def get_upper_ref(self, key, k):

        if key in self._upper_bounds[k][self._left_key]:
            return self._left_key

        return self._right_key

    def get_lower_ref(self, key, k):

        if key in self._lower_bounds[k][self._left_key]:
            return self._left_key

        return self._right_key
