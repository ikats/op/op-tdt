import gc
import random
import numpy as np
import tdt.decisiontree.core.LearningSetBuilder as Lsb


def cross_validation_one_fold(classifier,
                              learningset,
                              test_fold=None,
                              random_selection=False):

    ls_len = len(learningset)
    ts_len = test_fold * ls_len
    all_keys = set(range(ls_len))

    if random_selection:
        test_keys = set(random.sample(all_keys, ts_len))
    else:
        test_keys = set(range(ls_len - ts_len, ls_len))

    train_keys = all_keys - test_keys
    ls_builder = Lsb.LearningSetBuilder()
    train_set = ls_builder.build_from_learningset(orig_learningset=learningset, keys=train_keys)
    test_set = ls_builder.build_from_learningset(orig_learningset=learningset, keys=test_keys)
    gc.collect()

    tree = classifier.fit(learningset=train_set)
    return tree.predict(test_set)


def cross_validation_k_fold(classifier,
                            learningset,
                            k=1,
                            random_selection=False):

    ls_len = len(learningset)

    # The first n_samples % n_splits folds have size n_samples // n_splits + 1,
    # other folds have size n_samples // n_splits, where n_samples is the number of samples.

    first_folds_nb = ls_len % k
    test_len = int(ls_len / k)

    k_vals = [test_len + 1 for _ in range(first_folds_nb)]
    k_vals.extend([test_len for _ in range(first_folds_nb, k)])

    all_keys = set(range(ls_len))
    results = dict()

    for i, ts_len in enumerate(k_vals):
        if random_selection:
            test_keys = set(random.sample(all_keys, ts_len))
        else:
            start_index = i*ts_len
            end_index = start_index + ts_len
            if start_index == end_index:
                end_index += 1
            test_keys = set(range(start_index, end_index))

        train_keys = all_keys - test_keys
        ls_builder = Lsb.LearningSetBuilder()
        train_set = ls_builder.build_from_learningset(orig_learningset=learningset, keys=train_keys)
        test_set = ls_builder.build_from_learningset(orig_learningset=learningset, keys=test_keys)

        tree = classifier.fit(learningset=train_set)

        # Do what you want with the results
        results['fold_' + str(i + 1)] = tree.predict(test_set)
        gc.collect()

    return results


def train_test_resampling(classifier=None, learningset=None, testset=None, seed=0, k=1):

    labels_distribution = learningset.count_lb_stats()
    ls_builder = Lsb.LearningSetBuilder()
    all_set = ls_builder.build_from_merging(learningset, testset)
    label_indexes = {label: [] for label in learningset.labels_set}
    for ind, obj in enumerate(all_set):
        label_indexes[obj.label].append(ind)

    for fold_ind in range(5):
        train_keys = []
        test_keys = []
        for label, indexes in label_indexes.items():
            random.shuffle(indexes)
            train_keys += indexes[0:labels_distribution[label]]
            test_keys += indexes[labels_distribution[label]:]
        new_learningset = ls_builder.build_from_learningset(orig_learningset=all_set, keys=train_keys)
        new_testset = ls_builder.build_from_learningset(orig_learningset=all_set, keys=test_keys)
        tree = classifier.fit(learningset=new_learningset)
        tree.predict(new_testset)
        print('Done: ', tree.validation['accuracy'][0], int((len(tree.tree) - 1) / 2))
    print()
        # return tree
        # Do something about the results


def train_resampling(classifier=None, learningset=None, seed=0, k=1):

    labels_distribution = learningset.count_lb_stats()
    label_test_distribution = dict()
    for label in labels_distribution:
        label_test_distribution[label] = int(round(labels_distribution[label] / k))
        labels_distribution[label] -= label_test_distribution[label]
    ls_builder = Lsb.LearningSetBuilder()
    label_indexes = {label: [] for label in learningset.labels_set}
    for ind, obj in enumerate(learningset):
        label_indexes[obj.label].append(ind)
    best_tree = None
    best_acc = 0
    acc_list = []
    tree = None
    acc_cm = np.zeros((len(learningset.labels_set), len(learningset.labels_set)), dtype=int)
    for fold_ind in range(k):
        train_keys = []
        test_keys = []
        for label, indexes in label_indexes.items():
            random.shuffle(indexes)
            train_keys += indexes[0:labels_distribution[label]]
            test_keys += indexes[labels_distribution[label]:]
        new_learningset = ls_builder.build_from_learningset(orig_learningset=learningset, keys=train_keys)
        new_testset = ls_builder.build_from_learningset(orig_learningset=learningset, keys=test_keys)
        tree = classifier.fit(learningset=new_learningset)
        tree.predict(new_testset, method=['cross-validation', k])
        acc_list.append(tree.validation['accuracy'][0])
        if tree.validation['accuracy'][0] > best_acc:
            best_tree = tree
            best_acc = tree.validation['accuracy'][0]
        acc_cm += tree.validation['confusion-matrix'][1:]
        print('Done: ', tree.validation['accuracy'][0], tree.validation['confusion-matrix'], int((len(tree.tree) - 1) / 2))
    best_tree.validation['confusion-matrix'] = [tree.validation['confusion-matrix'][0][:]] + acc_cm.tolist()
    acc_std = np.round(np.std(acc_list), 4)
    acc_average = np.round(np.average(acc_list), 4)
    best_tree.validation['accuracy'][0] = str(acc_average) + ' ± ' + str(acc_std)

    return best_tree
