"""
Usage: DecisionTreeClassifier.py

DecisionTreeClassifier.py contains the implementation of the TDT algorithm
with its different options (Hyperplane, Spherical, Local_search and some other combination),
also the implementation deals with different variety of variables types:
(RHM patterns, standard variables, Timeseries, and sklearn).

Because of the many options this class contains, the gain calculation is done
on each node globally, meaning that we have one gain value that only changes
when a better gain is found.

"""

import copy
import datetime
import gc
import itertools
import sys
import numpy as np
from collections import OrderedDict
import sklearn.tree as sktree
import tdt.decisiontree.core.DecisionTree as Dt
import tdt.decisiontree.core.TreeNode as Tn
import tdt.decisiontree.core.Type as Tp
import tdt.decisiontree.core.Utility as Util
import tdt.decisiontree.core.Weighter as Wt
import tdt.decisiontree.core.MeasureData as Md
import tdt.decisiontree.core.WDMatrix as Wdm
import tdt.decisiontree.core.WeightObject as Wo

# Import logger
import logging

# Logger definition
LOGGER = logging.getLogger(__name__)


class DecisionTreeClassifier(object):
    """
    This is the main class for the Decision Tree algorithm,
    it contains the implementation of the algorithm.
    The implementation takes into account all types of variables (continuous, nominal, TS),
    and the generated tree is an R-Tree, (binary tree is the default option)
    """

    def __init__(self,
                 min_gain_percentage=0.00000000001,
                 split_criteria='gini',
                 distance_measure=None,
                 window=None,
                 method=None,
                 weight_type=None,
                 weight_factor=None,
                 store_path=False,
                 max_depth=None,
                 ts_splitter=2,
                 min_observations_node=2,
                 majority_class_percentage=0.98,
                 unbalanced_tree=False,
                 verbosity=False,
                 privacy=False,
                 sklearn=True,
                 search_percentage=1.0,
                 # local_search=False,
                 version=2.48,
                 builder='IKATS TDT builder',
                 source='IKATS Framework'):

        """
        :param min_gain_percentage: minimum gain for the node
        :param split_criteria: gini or entropy (can be updated)
        :param distance_measure: cort/DTW or norm [manhattan/euclidean] (can be updated)
        :param weight_type: the weighting method (mean, wb, ...)
        :param weight_factor: the weighting factor (sqrt, square, ...)
        :param store_path: the same path used to compute the weights is used in computing the distance
        :param max_depth: max depth of the tree
        :param ts_splitter: how many separators timeseries
        :param min_observations_node: minimum observations per node
        :param majority_class_percentage: Pre-pruning accuracy percentage
        :param verbosity: invoke debug mode
        :param privacy: output only the information needed for the visualization
        :param sklearn: use Sci-kit learn for continuous/nominal?/pattern variables.
        :param search_percentage: the percentage of distances to calculate (1.0 full search)
        # :param local_search: Invoke the local search (Elkan based) method
        """

        self._known_gain = {"continuous": self.gain_continuous, "nominal": self.gain_nominal,
                            "sklearn": self.gain_sklearn, "nominal_one": self.gain_nominal_one,
                            "pattern": self.gain_continuous, "timeseries": self.gain_timeseries}
        # if local_search:
        #      self._known_gain['timeseries'] = self.local_search
        # else:
        #     self._known_gain['timeseries'] = self.gain_timeseries

        self.min_gain_percentage = min_gain_percentage
        if max_depth is None:
            max_depth = sys.maxsize
        self.max_depth = max_depth
        self.min_observations_node = min_observations_node
        self.majority_class_percentage = majority_class_percentage
        self.sklearn = sklearn
        self.split_criteria = split_criteria
        if distance_measure is None:
            distance_measure = ['l2']
        if method is None:
            method = ['add']
        self.method = method
        self.distance_measure = distance_measure
        if weight_type is None:
            weight_type = ['uniform']
        self.weight_type = weight_type
        self.window = window
        self.store_path = store_path
        self.ts_splitter = ts_splitter
        self.unbalanced_tree = unbalanced_tree
        self.search_percentage = search_percentage
        self.verbosity = verbosity
        self.privacy = privacy
        self._gain = None

        # This class keeps internally the learningset it processes for a better use of memory
        self.learningset = None

        # Store the distances (Timeseries) and weights
        self.distances = None
        self.w_factors = {'normal': 1, 'square': 2, 'sqrt': 0.5}
        if weight_factor is None:
            weight_factor = ['normal']
        self.weight_factor = {f: self.w_factors[f] for f in weight_factor}

        # Internal use
        self.current_var_index = 0
        self.current_node = None
        self.best_gain = None
        self.max_margin = 0
        self.gain_changed = False
        self.use_dictionaries = self.search_percentage != 1.0
        self.stats_weights = None

        # Metadata
        self._version = version
        self._builder = builder
        self._source = source

    def initialize_process(self):

        data_length = len(self.learningset)
        ls_ids = list(range(data_length))

        # Get labels/class distribution for the root
        label_stats = self.learningset.count_lb_stats()

        # Whether to use the unbalanced distribution formula or not
        if self.unbalanced_tree:
            self.stats_weights = {label: 1/label_stats[label] for label in label_stats}
        else:
            self.stats_weights = {label: 1 for label in label_stats}

        # Get root gain and initialize the root TreeNode Object.
        ls_gain = Util.get_gain(self.split_criteria, stats=label_stats, stats_weights=self.stats_weights)
        root = Tn.TreeNode(set_ids=ls_ids, gain=ls_gain[1], label_stats=label_stats)

        # Calculating distances
        self.distances = []

        # To calculate the distance matrix, (weighted or not weighted, L1, L2, DTW, CorT or CorT_DTW),
        # the first thing we do is calculating the uniform non-weighted distance matrix,
        # then we calculate the weights, and finally the weighted distance matrix.
        # The implementation of the weights formulas can be found in the class Weighter

        # Initializing the Weighter object
        weighter = Wt.Weighter(learningset=self.learningset, weight_factor=self.weight_factor)

        # For each variable of type TIMESERIES, for each distance measure,
        # for each weight type and for each weight factor, calculate the distances
        for ind, v in enumerate(self.learningset.variables):
            LOGGER.debug('Compute the distance matrix and weights for variable %s ...', v.name)
            if v.imp_type.value == 'timeseries':
                for measure in self.distance_measure:
                    matrix = Md.MeasureData(var_id=ind, distance_measure=measure)
                    LOGGER.debug('Compute the uniform distance matrix ...')
                    self.compute_uniform_matrix(matrix)
                    self.distances.append(matrix)
                    temp_method = ['add'] if measure in ['l1', 'l2'] else self.method
                    if measure in ['cort', 'cort_dtw']:
                        continue
                    for wt in self.weight_type:
                        if wt == 'uniform':
                            continue
                        LOGGER.debug('Compute the weights ...')
                        weight_fac_dict = weighter.get_weights(matrix, wt)
                        for factor in weight_fac_dict:
                            for method in temp_method:
                                wd_matrix = Wdm.WDMatrix(method=method, weight_type=wt,
                                                         weight_factor=factor, weights=weight_fac_dict[factor])
                                LOGGER.debug('Compute the weighted distance matrix ...')
                                self.compute_weighted_matrix(matrix, wd_matrix)
                                matrix.data.append(wd_matrix)

        return root

    def gain(self, data, var_type):

        """
        Gain method hub relative to the variable type (Pattern, Variable, Timeseries)
        :param data: Array of values/timeseries to calculate its information gain
        :param var_type: current variable/variables type (sklearn, nominal, continuous, timeseries)
        """

        # var_type: sklearn deals with all continuous variables rather than one at a time.

        self._gain = self._known_gain[var_type]
        self._gain(data)

    def gain_nominal(self, data):

        """
        Calculate the gain and build the related node for a nominal variable (categorical values).
        The number of children nodes are equal to the number of categorical values.

        :param data: list of GainObject (feature/variable raw value, observation index, label)
        """

        children = []
        threshold = []

        # Getting the current variable data
        variable = self.learningset.variables[self.current_var_index]
        if variable.range is not None:
            variable.range = {o.distance for o in data}

        val_gain = 0
        data_length = len(data)

        # Set of the values
        for val in variable.range:

            # Get observations indexes with raw value equal to 'val'
            sub_keys = [o.obs_index for o in data if o.distance == val]
            # Get the labels distributions
            val_labels_stats = self.learningset.count_lb_stats(sub_keys)

            # Calculate the gain
            probability = len(sub_keys) / data_length
            child_gain = Util.get_gain(self.split_criteria, probability, val_labels_stats, self.stats_weights)
            val_gain += child_gain[0]

            # Store the results
            children.append(Tn.TreeNode(set_ids=sub_keys, gain=child_gain[1], label_stats=val_labels_stats))
            threshold.append(val)

        if val_gain < self.best_gain:
            # Store the split information
            self.best_gain = val_gain
            self.current_node.node_variable = variable
            self.current_node.data['threshold'] = ['='] * len(threshold)
            self.current_node.data['values'] = threshold
            self.current_node.children = children
            self.current_node.node_type = 'variable'

    def gain_nominal_one(self, data):

        """
        Calculate the gain and build the related node for a nominal variable (categorical values).
        The number of children nodes are 2, and it is dealt with using the one-against-all approach (no ordering).

        :param data: list of GainObject (feature/variable raw value, observation index, label)
        """

        data_length = len(data)
        variable = self.learningset.variables[self.current_var_index]
        if variable.range is not None:
            variable.range = {o.distance for o in data}

        # Set of the values
        for val in variable.range:
            keys = [[], []]
            for o in data:
                if o.distance == val:
                    keys[0].append(o.obs_index)
                else:
                    keys[1].append(o.obs_index)
            labels = [self.learningset.count_lb_stats(keys[0]), self.learningset.count_lb_stats(keys[1])]

            gain_info = self.get_gain_information(keys, data_length, labels)

            if gain_info[0] < self.best_gain:
                # Store the information and return it
                self.best_gain = gain_info[0]
                self.current_node.node_variable = variable
                self.current_node.data['values'] = [val, val]
                self.current_node.children = [Tn.TreeNode(keys[0], gain=gain_info[1],
                                                          label_stats=dict(labels[0])),
                                              Tn.TreeNode(keys[1], gain=gain_info[2],
                                                          label_stats=dict(labels[0]))]

                # Store the split information
                self.current_node.data['threshold'] = ['=', '!=']
                self.current_node.node_type = 'variable'

    def gain_sklearn(self, data):

        """
        Calculate the best gain for a list of continuous variables using sklearn.
        And since SKlearn doesn't deal with timeseries, we only run the DT of SKlearn for the node using max)depth=1,
        then we parse the results to know the best discriminant feature (variable).
        After that, we continue to calculate the gain for the rest of the features (Timeseries, nominal, ...)

        :param data: values of the observations for the related variable
        :type data: np.array
        """

        # Initialization of the data
        keys = self.current_node.set_ids
        data_length = len(self.current_node.set_ids)

        # random_state = 0 because we always want the same results at the end
        # max_depth = 1 because we only care about the current node.
        skl_tree = sktree.DecisionTreeClassifier(random_state=0, max_depth=1)
        data_labels = self.learningset.labels(keys)

        # Learn the tree
        skl_tree.fit(data, data_labels)
        # Get the nodes
        tree_nodes = skl_tree.tree_.__getstate__()['nodes']

        # Adapt the results to be compliant with our system.
        # Get the gain value from the sklearn tree
        if len(tree_nodes) > 1:
            # v[4] is the node gain without taking into account the probability factor
            # v[5] is the number of observations for a child node
            gain = sum(v[4] * (v[5]/data_length) for v in tree_nodes[1:])

            # Get the separation indexes into the two nodes
            split_leaves = skl_tree.apply(data)

            # Get the observations indexes
            keys = np.array(keys, dtype=int)
            l_keys = keys[list(np.where(split_leaves == 1)[0])]
            r_keys = keys[list(np.where(split_leaves == 2)[0])]

            # Get their labels
            l_labels = self.learningset.count_lb_stats(l_keys)
            r_labels = self.learningset.count_lb_stats(r_keys)

            if gain < self.best_gain and len(tree_nodes) > 1:
                self.best_gain = gain
                # tree_nodes[0][2] is the variable/feature index
                variables = self.learningset.variables[tree_nodes[0][2]]
                self.current_node.node_variable = variables
                self.current_node.data['threshold'] = ['<=', '>']
                self.current_node.data['values'] = [skl_tree.tree_.threshold[0], skl_tree.tree_.threshold[0]]
                self.current_node.children = [Tn.TreeNode(set_ids=l_keys, gain=tree_nodes[1][4], label_stats=l_labels),
                                              Tn.TreeNode(set_ids=r_keys, gain=tree_nodes[2][4], label_stats=r_labels)]
                if self.current_node.node_variable.imp_type.value == 'pattern':
                    self.current_node.node_type = 'pattern'
                else:
                    self.current_node.node_type = 'variable'

    def gain_continuous(self, data):

        """
        Calculate the gain and build the related node for a continuous/pattern variable.
        The number of the children nodes are 2.

        :param data: list of GainObject (feature/variable raw value, observation index, label)
        """

        variable = self.learningset.variables[self.current_var_index]

        # if the gain has changed, set the current node variable to continuous/pattern based on which input it is.
        self.gain_changed = False
        self.max_margin = 0
        if self.candidate_gain(data):
            self.current_node.node_variable = variable
            if variable.imp_type.value == 'pattern':
                self.current_node.node_type = 'pattern'
            else:
                self.current_node.node_type = 'variable'

    def gain_timeseries(self, data):

        """
        Calculate the gain and build the related node for timeseries.
        The number of children nodes is 2.

        :param data: list of GainObject (distance value set to 0, observation index, label)
        """

        variables = self.learningset.variables[self.current_var_index]

        # Spherical full search
        if self.ts_splitter < 3:
            LOGGER.debug('Searching for the best hypersphere ...')
            for mat_ind, matrix in enumerate(self.distances):
                if matrix.var_id != self.current_var_index:
                    continue
                for wd_data in matrix.data:
                    self.gain_changed = False
                    self.max_margin = 0
                    for ts in data:
                        # Calculate the distance array for the ts, data object is changed to store the distances
                        self.get_all_distances_spherical(ts, data, mat_ind, wd_data)
                        average_distance = round(sum([d.distance for d in data]) / len(data), 4)

                        # Deal with the distances as a continuous variable values
                        if self.candidate_gain(data):
                            ts_obj = self.learningset[ts.obs_index]
                            self.current_node.data['distance'] = [matrix.distance_measure, ['method', wd_data.method]]
                            self.current_node.data['average_distance'] = [average_distance]
                            self.current_node.data['raw_weight_sum'] = [wd_data.weights
                                                                        [ts.obs_index].unnormalized_sum]
                            self.current_node.data['weight'] = [wd_data.weight_type, wd_data.weight_factor]
                            self.current_node.node_type = 'similarity'
                            self.current_node.node_variable = variables
                            values = [lst for lst in ts_obj.data[self.current_var_index].data.tolist()]
                            names = ts_obj.data[self.current_var_index].name
                            tsu_ids = ts_obj.data[self.current_var_index].tsuid
                            self.current_node.data['sequences'] = \
                                [{'sequence': names,
                                  'tsuid': tsu_ids,
                                  'value': values,
                                  'observation': ts_obj.obs_id,
                                  'weight': list(wd_data.weights[ts.obs_index].data),
                                  'class': ts.label}]

        # Hyper-plane full search
        if self.ts_splitter > 1:
            LOGGER.debug('Searching for the best hyperplane ...')
            for mat_ind, matrix in enumerate(self.distances):
                if matrix.var_id != self.current_var_index:
                    continue
                for wd_data in matrix.data:
                    max_margin = 0
                    gain_changed = False
                    for ts_ls in itertools.combinations(data, 2):
                        temp_labels = set([v.label for v in ts_ls])
                        if len(temp_labels) <= 1:
                            continue

                        val_gain = 0
                        children = []

                        # Calculate the distances for all the separators and store them in array
                        dist_array = self.get_all_distances_hyperplane(ts_ls, mat_ind, wd_data)
                        average_distances = (
                        round(np.average(dist_array[0]), 4), round(np.average(dist_array[1]), 4))
                        current_margin = np.fabs(dist_array[0] - dist_array[1]).min()

                        # Find the similar timeseries for each ts candidate
                        min_array = np.argmin(dist_array, axis=0)

                        # Separate the observations into nodes
                        index_dict = dict()
                        for val in range(len(ts_ls)):
                            index_dict[val] = [data[ind].obs_index for ind in np.where(min_array == val)[0]]

                        # Calculate the gain
                        children_gain = dict()
                        for key, val in sorted(index_dict.items()):
                            val_labels_counter = self.learningset.count_lb_stats(val)
                            probability = (len(val) / len(data))
                            child_gain = Util.get_gain(self.split_criteria, probability,
                                                       val_labels_counter, self.stats_weights)
                            val_gain += child_gain[0]
                            children_gain[key] = (child_gain[1], val_labels_counter)

                        # Check the gain if the it best one yet and store its information.
                        margin_cond = val_gain == self.best_gain and current_margin > max_margin and gain_changed
                        if val_gain <= self.best_gain or margin_cond:
                            gain_changed = True
                            self.best_gain = val_gain
                            max_margin = current_margin
                            for key, val in sorted(index_dict.items()):
                                children.append(Tn.TreeNode(set_ids=val,
                                                            gain=children_gain[key][0],
                                                            label_stats=children_gain[key][1]))

                            sequences = []
                            un_sums = []
                            for val in ts_ls:
                                ts_obj = self.learningset[val.obs_index]
                                values = [lst for lst in ts_obj.data[self.current_var_index].data.tolist()]
                                names = ts_obj.data[self.current_var_index].name
                                tsu_ids = ts_obj.data[self.current_var_index].tsuid
                                un_sums.append(wd_data.weights[val.obs_index].unnormalized_sum)
                                sequences.append({'sequence': names,
                                                  'tsuid': tsu_ids,
                                                  'value': values,
                                                  'observation': ts_obj.obs_id,
                                                  'weight': list(wd_data.weights[val.obs_index].data),
                                                  'class': val.label})
                            self.current_node.node_variable = variables
                            self.current_node.data['sequences'] = sequences
                            self.current_node.children = children
                            self.current_node.data['distance'] = [matrix.distance_measure,
                                                                  ['method', wd_data.method]]
                            self.current_node.data['average_distance'] = average_distances
                            self.current_node.data['raw_weight_sum'] = un_sums
                            self.current_node.data['weight'] = [wd_data.weight_type, wd_data.weight_factor]
                            self.current_node.node_type = 'comparison'

    def fit(self, learningset):

        """
        Learn the tree from the learningset
        :return: A DecisionTree object
        """

        # Initialization of data:
        # 'unknown is for leaves with no discriminant class'

        LOGGER.debug('Initializing parameters and calculating distance matrix and weights...')
        self.learningset = learningset
        unknown = 'unknown'
        majority_class_msg = 'majority class '
        tree_depth = 0
        root = self.initialize_process()

        # One observation only
        if len(root.set_ids) < 1:
            return root

        # [(Depth, Node)]
        children_stack = [(0, root)]

        LOGGER.debug('searching for best tree ...')
        # Stop Criteria, (children to process, min_gain, depth, min_observation)
        while len(children_stack) > 0:
            LOGGER.debug('Processing a new node ...')
            root_vars_indexes = set(range(len(self.learningset.variables)))
            current_depth = children_stack[0][0]
            self.best_gain = 1

            self.current_node = children_stack[0][1]
            self.current_node.set_ids = list(self.current_node.set_ids)

            # Select the best label of the current node (balanced or unbalanced distribution)
            max_label = 0
            best_label = 'None'
            weighted_stats = {label: self.current_node.label_stats[label] * self.stats_weights[label]
                              for label in self.current_node.label_stats}
            for label in weighted_stats:
                if weighted_stats[label] > max_label:
                    max_label = weighted_stats[label]
                    best_label = label
                    self.current_node.data['label'] = best_label
                elif weighted_stats[label] == max_label:
                    best_label = unknown
                    self.current_node.data['label'] = unknown

            # Keep the statistics of each label
            self.current_node.data['depth'] = current_depth

            # Condition of Leaf Node
            min_nd_size_cd = len(self.current_node.set_ids) <= self.min_observations_node
            min_gain_cd = self.current_node.gain < self.min_gain_percentage
            depth_cd = current_depth >= self.max_depth
            node_size = sum(weighted_stats.values())

            if best_label == unknown:
                major_label_cd = False
            else:
                major_label_cd = (weighted_stats[best_label] / node_size) >= self.majority_class_percentage

            # Check if the current node should be a leaf node
            if min_nd_size_cd or major_label_cd or min_gain_cd or depth_cd:
                if current_depth > tree_depth:
                    tree_depth = current_depth
                children_stack = children_stack[1:]
                self.current_node.name = majority_class_msg + str(best_label)
                self.current_node.node_type = 'leaf'
                continue

            # If sklearn is used, get all the continuous/pattern variables, build a matrix of the values
            # and give it to sklearn.
            if self.sklearn:
                sklearn_indexes = [ind for ind, v in enumerate(self.learningset.variables)
                                   if v.imp_type == Tp.Type.CONTINUOUS or v.imp_type == Tp.Type.PATTERN]
                if len(sklearn_indexes) > 0:
                    data_var = self.learningset.data(sklearn_indexes, keys=self.current_node.set_ids, sklearn=True)
                    sklearn_indexes = set(sklearn_indexes)
                    self.gain(data=data_var, var_type='sklearn')
                    root_vars_indexes = root_vars_indexes - sklearn_indexes

            # For each variable (Timeseries, Nominal and Continuous/Pattern if sklearn is not used),
            # calculate the relative gain and keep the best one
            for var_ind in sorted(root_vars_indexes):
                self.current_var_index = var_ind
                data_var = self.learningset.data(self.current_var_index, keys=self.current_node.set_ids)
                self.gain(data=data_var, var_type=self.learningset.variables[var_ind].imp_type.value)

            if self.current_node.children is not None:
                # Moving on to the next node
                children_stack = children_stack[1:] + [(current_depth + 1, c) for c in self.current_node.children]
            else:
                self.current_node.name = majority_class_msg + str(best_label)
                self.current_node.node_type = 'leaf'
                # Moving on to the next node
                children_stack = children_stack[1:]

            gc.collect()

        LOGGER.debug('Building the tree representation ...')
        return self.build_tree(root, tree_depth)

    def get_all_distances_hyperplane(self, ts_ls, mat_ind, wd_data):
        """
        Return an array of array of distances related to the ts_ls at hand
        :param ts_ls: the candidate timeseries
        :param mat_ind: MeasureData index
        :param wd_data: The  related distance matrix
        :return: double array of distance values
        """

        array = []

        for ts_ref in ts_ls:

            ts_array = []

            if self.use_dictionaries:
                for ts_new in self.current_node.set_ids:
                    distance = self.get_distance(ts_ref.obs_index, ts_new, mat_ind, wd_data)
                    ts_array.append(distance)
            else:
                ts_array = wd_data.values[ts_ref.obs_index, self.current_node.set_ids]

            array.append(ts_array)

        return np.array(array)

    def get_all_distances_spherical(self, ts_ref, data, mat_ind, wd_data):

        """
        Return an array of array of distances related to the ts_ls at hand
        :param ts_ref: the separators timeseries
        :param data: GainObject array
        :param mat_ind: MeasureData index
        :param wd_data: The related distance matrix
        """

        if self.use_dictionaries:
            for ts_new in data:
                distance = self.get_distance(ts_ref.obs_index, ts_new.obs_index, mat_ind, wd_data)
                ts_new.distance = distance
        else:
            for ts_new in data:
                distance = wd_data.values[ts_ref.obs_index, ts_new.obs_index]
                ts_new.distance = distance

    def get_distance(self, ts1_ref, ts2_ref, mat_ind, wd_data):

        """
        Return the distance value between two timeseries
        :param ts1_ref: Key index for the first timeseries
        :param ts2_ref: Key index for the second timeseries
        :param mat_ind: index of the MeasureData object
        :param wd_data: index of the distance matrix
        :return: distance value
        :rtype: float
        """

        path = None
        switch_path = False

        if wd_data.weight_type == 'uniform' and wd_data.method:
            if ts1_ref < ts2_ref:
                distance_key = (ts1_ref, ts2_ref)
            else:
                distance_key = (ts2_ref, ts1_ref)
                if self.store_path:
                    switch_path = True
        else:
            distance_key = (ts1_ref, ts2_ref)

        # Same timeseries, distance --> 0
        if ts1_ref == ts2_ref:
            wd_data.values[distance_key] = 0
            return 0

        if distance_key in wd_data.values:
            return wd_data.values[distance_key]

        ptr_a = self.learningset[ts1_ref].data[self.current_var_index].data
        ptr_b = self.learningset[ts2_ref].data[self.current_var_index].data

        matrix = self.distances[mat_ind].distance_measure
        temp_weights = None
        if wd_data.weight_type != 'uniform':
            temp_weights = wd_data.weights[ts1_ref].data
        if self.store_path and matrix.distance_measure not in ['l1', 'l2']:
            path = self.distances[mat_ind].paths[distance_key]
        dist_val = Util.get_distance(x=ptr_a, y=ptr_b, w=temp_weights, path=path, window=self.window,
                                     switch_path=switch_path, measure=matrix.distance_measure)
        wd_data.values[distance_key] = dist_val

        return dist_val

    def compute_uniform_matrix(self, matrix):

        """
        Calculate the uniform distance matrix (additive and averaged),
        and depending on the input parameters, store or not the following:
        - Uniform additive distance matrix
        - Uniform averaged distance matrix
        - The temporal differences between two timeseries in case of L1 or L2
          e.g. given two timeseries TS1=[1, 2, 3] and TS2=[1, 1, 2], the temporal difference will be [0, 1, 1].
          The temporal difference will be used to calculate the weights of each timeseries.

        - The DTW, CorT, Cort_DTW path (mappting between two timeseries)

        :param matrix: a MeasureData object
        """

        ls_len = len(self.learningset)
        weight_dict = dict()
        if self.use_dictionaries:
            dist_mat_1 = dict()
        else:
            dist_mat_1 = np.zeros((ls_len, ls_len), dtype=np.float64)

        # If uniform is given as parameter, save the weights vectors as arrays of ones
        if 'uniform' in self.weight_type:
            weight_dict = {i: [] for i in range(ls_len)}
            for i in range(ls_len):
                values = np.ones(self.learningset[i].data[matrix.var_id].length, dtype=np.float64)
                weight_dict[i] = Wo.WeightObject(data=values,
                                                 unnormalized_sum=self.learningset[i].data[matrix.var_id].length)

        # If the distance measure is L1 or L2, additive and averaged distances are the same, only save additive,
        if matrix.distance_measure in ['l1', 'l2']:
            for i, j in itertools.combinations(range(ls_len), 2):
                data_1 = self.learningset[i].data[matrix.var_id].data
                data_2 = self.learningset[j].data[matrix.var_id].data
                dist_obj = Util.get_distance(data_1, data_2, window=self.window,
                                             measure=matrix.distance_measure, get_path=True)
                if self.use_dictionaries:
                    dist_mat_1[(i, j)] = dist_obj.additive_dist
                else:
                    dist_mat_1[i, j], dist_mat_1[j, i] = dist_obj.additive_dist, dist_obj.additive_dist
                matrix.paths[(i, j)] = dist_obj.path

            if 'uniform' in self.weight_type:
                matrix.data.append(Wdm.WDMatrix(method='add', weight_type='uniform', weight_factor='normal',
                                                values=dist_mat_1, weights=weight_dict))
        else:
            # Save two distance matrices (additive and averaged), it they were given as parameters.
            dist_mat_2 = np.zeros((ls_len, ls_len), dtype=np.float64)
            for i, j in itertools.combinations(range(ls_len), 2):
                data_1 = self.learningset[i].data[matrix.var_id].data
                data_2 = self.learningset[j].data[matrix.var_id].data
                dist_obj = Util.get_distance(data_1, data_2, window=self.window,
                                             measure=matrix.distance_measure, get_path=True)
                matrix.paths[(i, j)] = dist_obj.path
                if self.use_dictionaries:
                    dist_mat_1[(i, j)] = dist_obj.additive_dist
                    dist_mat_2[(i, j)] = dist_obj.average_dist_1
                    dist_mat_2[(j, i)] = dist_obj.average_dist_2
                else:
                    dist_mat_1[i, j], dist_mat_1[j, i] = dist_obj.additive_dist, dist_obj.additive_dist
                    dist_mat_2[i, j] = dist_obj.average_dist_1
                    dist_mat_2[j, i] = dist_obj.average_dist_2

            # Store the matrices if 'uniform' is given as an input parameter.
            if 'uniform' in self.weight_type:

                if 'add' in self.method:
                    matrix.data.append(Wdm.WDMatrix(method='add', weight_type='uniform', weight_factor='normal',
                                                    values=dist_mat_1, weights=weight_dict))
                if 'avg' in self.method:
                    matrix.data.append(Wdm.WDMatrix(method='avg', weight_type='uniform', weight_factor='normal',
                                                    values=dist_mat_2, weights=weight_dict))

    def compute_weighted_matrix(self, matrix, wd_matrix):

        """
        Computes the (full) weighted distance matrix.

        :param matrix: MeasureData object
        :param wd_matrix: WDMatrix
        """

        ls_len = len(self.learningset)

        if self.use_dictionaries:
            dist_mat = dict()
        else:
            dist_mat = np.zeros((ls_len, ls_len), dtype=np.float64)
        for i, j in itertools.combinations(range(ls_len), 2):
            data_1 = self.learningset[i].data[matrix.var_id].data
            data_2 = self.learningset[j].data[matrix.var_id].data
            path = None
            if self.store_path and matrix.distance_measure not in ['l1', 'l2']:
                path = matrix.paths[(i, j)]
            temp_weights_1 = wd_matrix.weights[i].data
            temp_weights_2 = wd_matrix.weights[j].data

            # calculate the weighted distance (according to ts_i with weight vector w_i)
            dist_obj_1 = Util.get_distance(data_1, data_2, w=temp_weights_1, window=self.window,
                                           measure=matrix.distance_measure, path=path)

            # calculate the weighted distance (according to ts_j with weight vector w_j)
            dist_obj_2 = Util.get_distance(data_2, data_1, w=temp_weights_2, window=self.window,
                                           measure=matrix.distance_measure, path=path, switch_path=True)
            if self.use_dictionaries:
                dist_mat[(j, i)] = dist_obj_1.additive_dist if wd_matrix.method == 'add' else dist_obj_1.average_dist_1
                dist_mat[(j, i)] = dist_obj_2.additive_dist if wd_matrix.method == 'add' else dist_obj_2.average_dist_1
            else:
                dist_mat[i, j] = dist_obj_1.additive_dist if wd_matrix.method == 'add' else dist_obj_1.average_dist_1
                dist_mat[j, i] = dist_obj_2.additive_dist if wd_matrix.method == 'add' else dist_obj_2.average_dist_1

        wd_matrix.values = dist_mat

    def candidate_gain(self, data):

        """
        Compute the gain for Continuous and similarity (HyperSphere) variables.
        It picks the threshold with the maximum margin.

        e.g. assuming the data are sorted:
        data =      [1, 1, 1, 3, 4, 6, 6, 9, 9, 9]
        labels =    [1, 1, 1, 2, 2, 2, 2, 3, 3, 3]

        In this case we will have two gain values that are the best with thresholds:
        - [2] and split ([1, 1, 1], [3, 4, 6, 6, 9, 9, 9])
        - [7.5] and split ([1, 1, 1, 3, 4, 6, 6], [9, 9, 9])
        Thus we pick the second split since the margin is 1.5 = (9 - 7.5) > margin 1 = (3 - 2)

        :param data: raw values
        """

        data_length = len(data)
        sorted_data = sorted(data)
        labels = [dict(), copy.deepcopy(self.current_node.label_stats)]
        keys = [set(), copy.deepcopy(set(self.current_node.set_ids))]
        for label in labels[1]:
            labels[0][label] = 0

        current_ind = 0
        u_bound_set = sorted(set([obj.distance for obj in sorted_data]))
        set_length = len(u_bound_set)
        current_set_ind = 0
        current_margin = 0
        if set_length == 1:
            current_threshold = u_bound_set[0]
        else:
            current_threshold = (u_bound_set[current_set_ind] + u_bound_set[current_set_ind + 1]) / 2
            current_margin = u_bound_set[current_set_ind + 1] - current_threshold
        gain_changed_locally = False
        threshold = ['<=', '>']
        while current_ind < data_length:
            if sorted_data[current_ind].distance <= current_threshold:
                keys[0].add(sorted_data[current_ind].obs_index)
                keys[1].remove(sorted_data[current_ind].obs_index)
                labels[0][sorted_data[current_ind].label] += 1
                labels[1][sorted_data[current_ind].label] -= 1
                current_ind += 1

            else:
                gain_info = self.get_gain_information(keys, data_length, labels)
                current_set_ind += 1
                margin_cond = gain_info[0] == self.best_gain and current_margin > self.max_margin and self.gain_changed
                if gain_info[0] <=  self.best_gain or margin_cond:
                    self.gain_changed = True
                    gain_changed_locally = True
                    self.max_margin = current_margin
                    self.best_gain = gain_info[0]
                    self.current_node.data['threshold'] = threshold
                    self.current_node.data['values'] = [current_threshold, current_threshold]
                    l_keys = copy.deepcopy(sorted(keys[0]))
                    r_keys = copy.deepcopy(sorted(keys[1]))
                    self.current_node.children = [Tn.TreeNode(set_ids=l_keys, gain=gain_info[1],
                                                              label_stats=copy.deepcopy(labels[0])),
                                                  Tn.TreeNode(set_ids=r_keys, gain=gain_info[2],
                                                              label_stats=copy.deepcopy(labels[1]))]

                if current_set_ind < set_length - 1:
                    sum_distance = u_bound_set[current_set_ind] + u_bound_set[current_set_ind + 1]
                    current_threshold = sum_distance / 2
                    current_margin = u_bound_set[current_set_ind + 1] - current_threshold
                else:
                    break

        return gain_changed_locally

    def build_tree(self, root, tree_depth):

        """
        Create a DecisionTree object from the results of the builder function.
        The Tree is based on a structure that will be easily exported to a json file,
        and used for prediction. (For more information, check the tree_export_format manual)
        :param root: Root of the Tree (TreeNode)
        :param tree_depth: Depth of the tree
        :return: a DecisionTree object
        """

        last_node_id = 0
        nodes = [root]
        node_id = 0
        nodes_output = []
        data = dict()
        sequences_set = set()

        while len(nodes) > 0:

            current_node = nodes[0]
            node_dict = OrderedDict()

            node_dict['node'] = node_id

            if current_node.node_type != 'leaf':
                if type(current_node.node_variable) == np.ndarray:
                    names = [v.name for v in current_node.node_variable]
                else:
                    names = [current_node.node_variable.name]
                node_dict['name'] = current_node.node_type + ': ' + '/'.join(names)
            else:
                node_dict['name'] = current_node.name
            node_dict['type'] = current_node.node_type
            node_dict['evaluation'] = [self.split_criteria, current_node.gain]

            statistics = current_node.label_stats
            node_dict['statistics'] = [[label, int(statistics[label])]
                                       for label in statistics if statistics[label] != 0]

            if current_node.node_type == 'leaf':
                if not self.privacy:
                    node_dict['observations'] = list(self.learningset.get_observations_ids(current_node.set_ids))
            else:
                node_dict['description'] = current_node.get_exporter(last_node_id)
                variable_id = self.learningset.var_ind_mapper[current_node.node_variable.name]
                node_dict['description']['variable_id'] = variable_id
                nb_children = len(current_node.children)
                nodes = nodes + current_node.children
                last_node_id += nb_children

            if 'sequences' in current_node.data:
                for sequence in current_node.data['sequences']:
                    for ind, ts_name in enumerate(sequence['sequence']):
                        if ts_name in sequences_set:
                            data[ts_name]['weight'].append([node_id, sequence['weight']])
                            continue
                        else:
                            sequence['weight'] = [[node_id, sequence['weight']]]
                            sequences_set.add(ts_name)
                            sequence_copy = copy.deepcopy(sequence)
                            sequence_copy['value'] = sequence_copy['value']
                            sequence_copy['sequence'] = ts_name
                            sequence_copy['tsuid'] = sequence_copy['tsuid']
                            data[ts_name] = sequence_copy

            node_id += 1

            nodes_output.append(node_dict)
            nodes = nodes[1:]

        header = {'TDTFormatVersion': self._version,
                  'builder': self._builder,
                  'date': datetime.datetime.today().strftime('%Y-%m-%d'),
                  'learningSet': self.learningset.name,
                  'source': self._source}

        data = list(data.values())
        return Dt.DecisionTree(header=header, tree=nodes_output,
                               data=data, depth=tree_depth, arguments=self.arguments)

    # Utility
    def get_gain_information(self, split_keys, data_length, label_stats):

        # Calculate the probability of the labels
        l_probability = (len(split_keys[0]) / data_length)
        r_probability = (len(split_keys[1]) / data_length)

        # Get the gain values (true_gain, node_gain)
        l_child_gain = Util.get_gain(self.split_criteria, l_probability, label_stats[0], self.stats_weights)
        r_child_gain = Util.get_gain(self.split_criteria, r_probability, label_stats[1], self.stats_weights)

        # Calculate the pure gain of the current gain
        gain = l_child_gain[0] + r_child_gain[0]

        return gain, l_child_gain[1], r_child_gain[1]

    def maximum_ts_length(self, ts_1, ts_2, ts_3=None):

        len_ts_1 = len(self.learningset[ts_1].data[self.current_var_index])
        len_ts_2 = len(self.learningset[ts_2].data[self.current_var_index])

        if ts_3 is None:
            return max(len_ts_1, len_ts_2)
        else:
            len_ts_3 = len(self.learningset[ts_3].data[self.current_var_index])

        return max(len_ts_1, len_ts_2, len_ts_3)

    @property
    def arguments(self):

        return [["minimum-gain", self.min_gain_percentage], ["split-criteria", self.split_criteria],
                ["min-obs/nodes", self.min_observations_node], ["max-cov%", self.majority_class_percentage * 100],
                ["store-path", self.store_path], ["window", self.window], ["unbalanced-tree", self.unbalanced_tree]]
