
"""
The LocalSearch class contains the implementation of the the local search algorithm
proposed by V. Shalaeva.
The class takes as input: 1) DecistionTreeClassifier, 2) a set of parameters
"""

import copy
import itertools
import sys
import time

import tdt.decisiontree.backup.UtilityV1 as Util
import tdt.decisiontree.core.GainObject as Go
import tdt.decisiontree.core.Seed as Sd
import tdt.decisiontree.core.TreeNode as Tn


class LocalSearch(object):

    def __init__(self, dt_classifier, data=None):

        self._dt_classifier = dt_classifier
        self._data = data

    def local_search(self, data, nb_init_seeds=1, m_dist=1, run_time=1, nn=5):

        """
        Local search is a greedy algorithm to build a temporal decision tree.
        The tree takes as an input either maximum number of resources (#distance calculation)
        or maximum running time.

        :param data: the timeseries raw data
        :param nb_init_seeds: number of initials seeds to explore
        :param m_dist: number of m_dist resources
        :param run_time: maximum running time for the algorithm
        :param nn: number of nearest neighbours to explore
        :return: DecisionTree
        """

        t_distance = 0.00001
        data_length = len(data)
        delta_time = 1
        recommended_time = data_length * t_distance * nb_init_seeds * nn + delta_time
        if run_time is None:
            run_time = m_dist * t_distance + delta_time
            if run_time < recommended_time:

                # Raise Some Error
                return None
        else:
            run_time = (run_time / t_distance) + delta_time
            if run_time < recommended_time:

                # Raise Some Error
                return None

        # Seed generator returns a random set of keys of length nb_init_seeds
        seed_keys = Util.rand_ref_generator(data, nb_init_seeds, self._dt_classifier.current_node.label_stats)

        # Generating the seed list from the seeds keys for both Hyperplane and Spherical plane
        seed_list = []
        if self._dt_classifier.all_types or self._dt_classifier.ts_nb_separators == 1:
            seed_list = [Sd.Seed(key=[data[key].obs_index]) for key in seed_keys]
        if self._dt_classifier.all_types or self._dt_classifier.ts_nb_separators == 2:
            for key_1, key_2 in itertools.combinations(seed_keys, 2):
                if data[key_1].label == data[key_2].label:
                    continue
                key = [key_1, key_2]
                seed_list.append(Sd.Seed(key=data[key].obs_index, is_hyper=True))

        # Initializing the seeds (calculating the gini of each seed)
        temp_time = time.process_time()
        for seed in seed_list:
            if seed.is_hyper:
                self.hyper_initial_assignment(seed, data, nn)
            else:
                self.spherical_initial_assignment(seed, data, nn)
        # run_time -= time.process_time() - temp_time

        processed_seeds = set()
        best_seed = seed_list[0]
        while run_time > 0 and len(seed_list) > 0:

            # Get best seed in term of gini
            seed_list.sort()
            current_best_seed = seed_list[0]

            # Exploring the current_best_seed neighbour
            for neighbour in current_best_seed.n_neighbours:
                if str(neighbour) in processed_seeds:
                    continue
                if current_best_seed.is_hyper:
                    self.hyper_update_assignment(neighbour, nn)
                    seed_list.append(neighbour)
                    processed_seeds.add(str(neighbour))
                else:
                    if self.spherical_update_assignment(neighbour, nn):
                        seed_list.append(neighbour)
                        processed_seeds.add(str(neighbour))

            seed_list = seed_list[1:]
            processed_seeds.add(str(current_best_seed))

            # Keep the best seed
            if current_best_seed.gain < best_seed.gain:
                best_seed = current_best_seed

        # Store the information of the current node.
        if best_seed.gain[0] < self._dt_classifier.best_gain:
            variables = self._dt_classifier.learningset.variables[self._dt_classifier.current_var_index]
            children = [Tn.TreeNode(set_ids=best_seed.split_keys[0], gain=best_seed.gain[1],
                                    label_stats=best_seed.label_stats[0]),
                        Tn.TreeNode(set_ids=best_seed.split_keys[1], gain=best_seed.gain[2],
                                    label_stats=best_seed.label_stats[1])]

            sequences = []
            for val in best_seed.key:
                for var_ind in self._dt_classifier.current_var_index:
                    ts_name = self._dt_classifier.learningset.get_ts_name(val, var_ind)
                    observation_data = self._dt_classifier.learningset[val].data[var_ind]
                    sequences.append({'sequence': ts_name,
                                      'value': list(observation_data),
                                      'observation': self._dt_classifier.learningset[val].obs_id,
                                      'class': self._dt_classifier.learningset[val].label})
            self._dt_classifier.current_node.node_variable = variables
            self._dt_classifier.current_node.data['sequence'] = sequences
            self._dt_classifier.current_node.children = children
            self._dt_classifier.current_node.data['distance'] = [self._dt_classifier.distance_measure, self._k[0]]
            if best_seed.is_hyper:
                self._dt_classifier.current_node.data['type'] = 'comparison'
            else:
                self._dt_classifier.current_node.data['type'] = 'similarity'
                self._dt_classifier.current_node.data['threshold'] = ['<=', '>']
                self._dt_classifier.current_node.data['values'] = [best_seed.split_value, best_seed.split_value]

    def hyper_initial_assignment(self, seed, data, nn):

        """
        Calculate the gini for an initial Hyperplane seed (pair of timeseries)

        :param seed: Seed object
        :param data: An array of GainObject
        :param nn: number of neighbours to explore

        Update the seed with the split (gain, children, ...)
        """

        seed.lower_bounds = copy.deepcopy(data)
        seed.upper_bounds = copy.deepcopy(data)
        data_length = len(data)

        # Initialization of the split
        labels = [dict(), dict()]
        keys = [set(), set()]
        k = self._k[0]
        for label in self._dt_classifier.learningset.labels_set:
            labels[0][label] = 0
            labels[1][label] = 0

        # Seed distance
        seed_distance = self._dt_classifier.get_distance(seed.key[0], seed.key[1], k)
        seed.split_value = seed_distance / 2

        for ind, ts in enumerate(data):
            left_is_upper = False
            l_dist = self._dt_classifier.get_distance(seed.key[0], ts.obs_index, k)
            r_dist = 0
            if l_dist <= seed.split_value:
                left_is_upper = True
            else:
                r_dist = self._dt_classifier.get_distance(seed.key[1], ts.obs_index, k)
                if l_dist <= r_dist:
                    left_is_upper = True

            # Upper and lower bounds assignment
            if left_is_upper:
                seed.upper_bounds[ind].partition = True
                seed.upper_bounds[ind].distance = l_dist
                seed.lower_bounds[ind].distance = r_dist
                keys[0].add(ts.obs_index)
                labels[0][ts.label] += 1
            else:
                seed.upper_bounds[ind].distance = r_dist
                seed.lower_bounds[ind].distance = l_dist
                keys[1].add(ts.obs_index)
                labels[1][ts.label] += 1

        # Calculate the gain
        seed.gain = self._dt_classifier.get_gain_information(keys, data_length, labels)
        seed.label_stats = labels
        seed.split_keys = keys

        # Get the nearest neighbours
        self.get_nearest_neighbours(seed, nn)

    def hyper_update_assignment(self, seed, nn):

        # Copy the upper and lower bounds, label_stats and split_keys
        seed.upper_bounds = copy.deepcopy(seed.parent_seed.upper_bounds)
        seed.lower_bounds = copy.deepcopy(seed.parent_seed.lower_bounds)
        seed.label_stats = copy.deepcopy(seed.parent_seed.label_stats)
        seed.split_keys = copy.deepcopy(seed.parent_seed.split_keys)

        data_length = len(seed.upper_bounds)
        swaps = 0
        k = self._k[0]

        # Calculate/get the seed distance
        seed_distance = self._dt_classifier.get_distance(seed.key[0], seed.key[1], k)
        seed.split_value = seed_distance / 2

        # Check if the left key is the same of that of the parent
        same_left = True
        if seed.key[0] != seed.parent_seed.key[0]:
            same_left = False

        for ind, bound in enumerate(seed.upper_bounds):
            lower_bound = seed.lower_bounds[ind]
            ts_length = self._dt_classifier.maximum_ts_length(seed.key[0], seed.key[1], bound.obs_index)

            # Check which distance to update using Elkan update
            if (bound.partition and not same_left) or (not bound.partition and same_left):
                bound.distance = Util.update_upper_bound(self._dt_classifier.distance_measure, bound.distance,
                                                         seed.update_value, ts_length)
            else:
                temp_val = Util.update_lower_bound(self._dt_classifier.distance_measure, bound.distance,
                                                   seed.update_value, ts_length)
                lower_bound.distance = max(0, temp_val)

            # First check for swapping
            if bound.distance > seed.split_value and bound.distance > lower_bound.distance:
                if bound.partition:
                    seed_key = seed.key[0]
                else:
                    seed_key = seed.key[1]
                bound.distance = self._dt_classifier.get_distance(bound.obs_index, seed_key, k)

                # Second check for swapping
                if bound.distance > seed.split_value and bound.distance > lower_bound.distance:
                    if bound.partition:
                        seed_key = seed.key[1]
                    else:
                        seed_key = seed.key[0]
                    lower_bound.distance = self._dt_classifier.get_distance(bound.obs_index, seed_key, k)

                # If there is a swap, change the partition
                if bound.distance > lower_bound.distance:
                    swaps += 1
                    temp_distance = lower_bound.distance
                    lower_bound.distance = bound.distance
                    bound.partition = not bound.partition
                    bound.distance = temp_distance
                    left, right = 1, 0
                    if bound.partition:
                        left, right = 0, 1
                    seed.label_stats[left][bound.label] += 1
                    seed.label_stats[right][bound.label] -= 1
                    seed.split_keys[left].add(bound.obs_index)
                    seed.split_keys[right].remove(bound.obs_index)

        # Calculate the gain
        seed.gain = self._dt_classifier.get_gain_information(seed.split_keys, data_length, seed.label_stats)
        # Get the nearest neighbours
        self.get_nearest_neighbours(seed, nn)

    def spherical_initial_assignment(self, seed, data, nn):

        # Data initialization
        k = self._k[0]
        seed_key = Go.GainObject(obs_index=seed.key[0])
        self._dt_classifier.get_all_distances_spherical(seed_key, data, k)
        seed.lower_bounds = copy.deepcopy(data)
        seed.upper_bounds = copy.deepcopy(data)
        data_length = len(data)

        # Sort data in term of distances
        sorted_data = sorted(data)
        labels = [dict(), copy.deepcopy(self._dt_classifier.current_node.label_stats)]
        keys = [set(), copy.deepcopy(set(self._dt_classifier.current_node.set_ids))]
        seed.gain = [1, 0, 0]
        for label in labels[1]:
            labels[0][label] = 0
        current_ind = 0
        current_threshold = (sorted_data[current_ind].distance + sorted_data[current_ind + 1].distance) / 2

        # Scan the threshold one by one until to find the best split
        while current_ind < data_length:
            if sorted_data[current_ind].distance <= current_threshold:
                keys[0].add(sorted_data[current_ind].obs_index)
                keys[1].remove(sorted_data[current_ind].obs_index)
                labels[0][sorted_data[current_ind].label] += 1
                labels[1][sorted_data[current_ind].label] -= 1
                current_ind += 1

            else:
                gain_info = self._dt_classifier.get_gain_information(keys, data_length, labels)

                if gain_info[0] < seed.gain[0]:
                    # Store the information and return it
                    seed.gain = gain_info
                    seed.split_value = current_threshold
                    seed.margin = abs(sorted_data[current_ind].distance - current_threshold)
                    seed.label_stats = (copy.deepcopy(labels))
                    seed.split_keys = (copy.deepcopy(keys))
                if current_ind < data_length - 1:
                    current_threshold = (sorted_data[current_ind].distance + sorted_data[current_ind + 1].distance) / 2
                else:
                    break

        # Update the best split partition
        for ind, obj in enumerate(seed.upper_bounds):
            if obj.obs_index in seed.split_keys[0]:
                obj.partition = True

        # Get the nearest neighbours
        self.get_nearest_neighbours(seed, nn)

    def spherical_update_assignment(self, seed, nn):

        # If the update value is less than the margin, no changes are made
        if seed.update_value < seed.parent_seed.margin:
            return False

        # Copy the lower bounds, upper bounds, label stats and split keys
        k = self._k[0]
        seed.upper_bounds = copy.deepcopy(seed.parent_seed.upper_bounds)
        seed.lower_bounds = copy.deepcopy(seed.parent_seed.lower_bounds)
        seed.label_stats = copy.deepcopy(seed.parent_seed.label_stats)
        seed.split_keys = copy.deepcopy(seed.parent_seed.split_keys)

        converge_cond = False
        while not converge_cond:
            min_distance = sys.float_info.max
            max_distance = 0
            nb_swaps = 0

            for ind, bound in enumerate(seed.upper_bounds):
                lower_bound = seed.lower_bounds[ind]
                if bound.obs_index == seed.key[0]:
                    bound.distance = 0
                    lower_bound.distance = 0
                    continue

                # Update distance using Elkan update method
                ts_length = self._dt_classifier.maximum_ts_length(seed.key[0], bound.obs_index)
                temp_val = Util.update_lower_bound(self._dt_classifier.distance_measure, bound.distance,
                                                   seed.update_value, ts_length)
                lb_update = max(0, temp_val)
                ub_update = Util.update_upper_bound(self._dt_classifier.distance_measure, bound.distance,
                                                    seed.update_value, ts_length)
                parent_threshold = seed.parent_seed.split_value

                upper_check = bound.distance < parent_threshold < ub_update
                lower_check = lb_update < parent_threshold < lower_bound.distance

                # Check for swaps
                if upper_check or lower_check:
                    temp_dist = self._dt_classifier.get_distance(seed.key[0], bound.obs_index, k)
                    bound.distance, lower_bound.distance = (temp_dist, temp_dist)
                    swap_upper_check = upper_check and temp_dist > parent_threshold
                    swap_lower_check = lower_check and temp_dist < parent_threshold

                    # If there is a swap, save the min and max distance
                    if swap_lower_check or swap_upper_check:
                        nb_swaps += 1
                        if temp_dist > max_distance:
                            max_distance = temp_dist
                        if temp_dist < min_distance:
                            min_distance = temp_dist

                else:
                    bound.distance = ub_update
                    lower_bound.distance = lb_update

            # Converge or re-calculate the new split threshold
            if nb_swaps > 0:
                self.spherical_threshold_update(seed, min_distance, max_distance)
            else:
                converge_cond = True

        # Get the nearest neighbours
        self.get_nearest_neighbours(seed, nn)
        return True

    def spherical_threshold_update(self, seed, min_distance, max_distance):

        # No need to copy the lower and upper bounds, the update is final here.
        # But we need to recalculate the label stats nad split keys
        data_length = len(seed.upper_bounds)
        labels = [dict(), copy.deepcopy(self._dt_classifier.current_node.label_stats)]
        keys = [set(), copy.deepcopy(set(self._dt_classifier.current_node.set_ids))]
        for label in labels[1]:
            labels[0][label] = 0

        sorted_data = sorted(seed.upper_bounds)
        u_bound_set = sorted(set([v.distance for v in sorted_data if min_distance <= v.distance <= max_distance]))
        set_length = len(u_bound_set)
        current_ind = 0
        current_set_ind = 0

        # No changes
        if set_length == 0:
            return

        if set_length == 1:
            current_threshold = u_bound_set[0]
        else:
            current_threshold = (u_bound_set[current_set_ind] + u_bound_set[current_set_ind + 1]) / 2

        # Scan the threshold one by one until to find the best split
        while current_ind < data_length:
            if sorted_data[current_ind].distance <= current_threshold:
                keys[0].add(sorted_data[current_ind].obs_index)
                keys[1].remove(sorted_data[current_ind].obs_index)
                labels[0][sorted_data[current_ind].label] += 1
                labels[1][sorted_data[current_ind].label] -= 1
                current_ind += 1

            else:
                gain_info = self._dt_classifier.get_gain_information(keys, data_length, labels)
                current_set_ind += 1

                if gain_info[0] < seed.gain[0]:
                    # Store the information and return it
                    seed.gain = gain_info
                    seed.split_value = current_threshold
                    seed.margin = abs(sorted_data[current_ind].distance - current_threshold)
                    seed.label_stats = (copy.deepcopy(labels))
                    seed.split_keys = (copy.deepcopy(keys))

                if current_set_ind < set_length - 1:
                    sum_distance = u_bound_set[current_set_ind] + u_bound_set[current_set_ind + 1]
                    current_threshold = sum_distance / 2
                else:
                    break

        # Update the best split partition
        for obj in seed.upper_bounds:
            if obj.obs_index in seed.split_keys[0]:
                obj.partition = True
            else:
                obj.partition = False

    def get_nearest_neighbours(self, seed, nn):

        neighbours = []
        nn_count = 1
        for nearest_n in sorted(seed.upper_bounds):
            if nearest_n.obs_index in seed.key:
                continue
            if seed.is_hyper:
                if nearest_n.partition:
                    key = [seed.key[0], nearest_n.obs_index]
                else:
                    key = [nearest_n.obs_index, seed.key[1]]
            else:
                key = [nearest_n.obs_index]
            neighbours.append(Sd.Seed(key=key,
                                      depth=seed.depth,
                                      parent_seed=seed,
                                      update_value=nearest_n.distance,
                                      is_hyper=seed.is_hyper))
            nn_count += 1
            if nn_count > nn:
                break
        seed.n_neighbours = neighbours
