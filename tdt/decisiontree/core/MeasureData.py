class MeasureData(object):

    def __init__(self,
                 var_id=None,
                 distance_measure=None,
                 paths=None,
                 data=None):

        self._distance_measure = distance_measure
        self._var_id = var_id

        if paths is None:
            paths = dict()
        self._paths = paths

        if data is None:
            data = []
        self._data = data

    @property
    def distance_measure(self):
        return self._distance_measure

    @distance_measure.setter
    def distance_measure(self, value):
        self._distance_measure = value

    @property
    def var_id(self):
        return self._var_id

    @var_id.setter
    def var_id(self, value):
        self._var_id = value

    @property
    def paths(self):
        return self._paths

    @paths.setter
    def paths(self, value):
        self._paths = value

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value
