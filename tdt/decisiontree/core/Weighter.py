import itertools
import gc
import copy
import math
import numpy as np
import tdt.decisiontree.core.WeightObject as Wo

"""
This class contains the implementation of the weighting approaches proposed by LIG.
Currently we only use the B/W (between/within) approach, other approaches are in the backup files. 
"""


class Weighter(object):
    def __init__(self,
                 learningset,
                 use_smoothing=True,
                 discretize=False,
                 smooth_window=2,
                 bins=3,
                 chunks=25,
                 ki=0.5,
                 weight_factor=None):

        """

        :param learningset: LearningSet object
        :param use_smoothing: Whether to use smoothing or not
        :param smooth_window: a percent of ts length to be used in smoothing the weights
        :param bins: How many bins to use when discretizing the smoothed weights
        :param chunks: a percent of ts length to be used in the hash-table approach
        """

        self.learningset = learningset
        self.weighter = {'l1': {'bw': self.bw_based_euclidean_weight},
                         'l2': {'bw': self.bw_based_euclidean_weight},
                         'dtw': {'bw': self.bw_based_dtw_weight}}
        self.use_smoothing = use_smoothing
        self.smoothing_window = smooth_window
        self.discretize = discretize
        self.bins = bins
        self.chunks = chunks
        self.weight_factor = weight_factor
        self.weight_type = None
        self.md_matrix = None
        self.data_length = len(learningset)
        self.ki = ki

    def get_weights(self, md_matrix, weight_type):

        """
        Update the md_matrix to include the requested weights vectors.
        :param md_matrix: WDMatrix object
        :param weight_type: currently only Between/Within ('bw') is included
        :return:
        """

        self.md_matrix = md_matrix

        return self.weighter[md_matrix.distance_measure][weight_type]()

    def bw_based_euclidean_weight(self):

        """
        Weighting time-series based on the between/withing approach using euclidean distance
        :return: a weight dictionary (obs_id --> weight vector)
        """

        labels_indexes = {i: [] for i in self.learningset.labels_set}
        label_probability_dict = {i: 0 for i in self.learningset.labels_set}

        # Initialization of the label probabilities
        for ind, ts in enumerate(self.learningset.observations):
            labels_indexes[ts.label].append(ind)
            label_probability_dict[ts.label] += 1
        for label in label_probability_dict:
            label_count = label_probability_dict[label]
            label_probability_dict[label] = label_count * math.exp(-1 * self.ki * math.log(label_count))
        ki_sum = sum(label_probability_dict.values())
        label_probability_dict = {i: label_probability_dict[i] / ki_sum for i in label_probability_dict}
        sorted_labels = sorted(self.learningset.labels_set)

        # Get the temporal differences
        weight_dict = self.get_temporal_difference()

        # Calculate the Between/Within weights (for more information, check the description file)
        for ts_id, ts_weight_mat in weight_dict.items():
            ts = self.learningset[ts_id].data[self.md_matrix.var_id]
            ts_weights = np.zeros(ts.length, dtype=np.float64)
            for timestamp in range(ts.length):
                timestamp_difference = ts_weight_mat[:, timestamp]
                global_mean = 0
                all_means = dict()
                between_variance = 0
                within_variance = 0
                for label in sorted_labels:
                    label_differences = timestamp_difference[labels_indexes[label]]
                    label_mean = np.mean(label_differences)
                    all_means[label] = label_mean
                    global_mean += label_probability_dict[label] * label_mean
                    label_sdv = np.var(label_differences)
                    within_variance += label_probability_dict[label] * label_sdv
                for label in sorted_labels:
                    between_variance += label_probability_dict[label] * math.pow(all_means[label] - global_mean, 2)
                if within_variance == 0:
                    within_variance = 1
                ts_weights[timestamp] = between_variance / within_variance

            weight_dict[ts_id] = ts_weights

        if self.use_smoothing:
            weight_dict = self.smooth(weight_dict)
        else:
            weight_dict = self.normalize(weight_dict)

        gc.collect()

        return weight_dict

    def bw_based_dtw_weight(self):

        """
        Weighting time-series based on the between/withing approach using DTW

        :return: a weight dictionary (obs_id --> weight vector)
        """

        # Initialization of the label probabilities
        label_probability_dict = {i: 0 for i in self.learningset.labels_set}
        for ind, ts in enumerate(self.learningset.observations):
            label_probability_dict[ts.label] += 1
        for label in label_probability_dict:
            label_count = label_probability_dict[label]
            label_probability_dict[label] = label_count * math.exp(-1 * self.ki * math.log(label_count))
        ki_sum = sum(label_probability_dict.values())
        label_probability_dict = {i: label_probability_dict[i] / ki_sum for i in label_probability_dict}
        sorted_labels = sorted(self.learningset.labels_set)

        # Get the temporal differences
        weight_dict = self.get_temporal_difference()

        # Calculate the Between/Within weights (for more information, check the description file)
        for ts_id in range(self.data_length):
            ts = self.learningset[ts_id].data[self.md_matrix.var_id]
            ts_weights = weight_dict[ts_id]
            for timestamp in range(ts.length):
                between_variance = 0
                within_variance = 0
                global_mean = 0
                all_means = dict()
                for label in sorted_labels:
                    label_difference = ts_weights[timestamp][label]
                    if len(label_difference) == 0:
                        label_difference = [0]
                    label_mean = np.mean(label_difference)
                    all_means[label] = label_mean
                    global_mean += label_probability_dict[label] * label_mean
                    label_sdv = np.var(label_difference)
                    within_variance += label_probability_dict[label] * label_sdv
                for label in sorted_labels:
                    between_variance += label_probability_dict[label] * math.pow(all_means[label] - global_mean, 2)
                if within_variance == 0:
                    within_variance = 1
                ts_weights[timestamp] = between_variance / within_variance

            ts_weights = np.array(ts_weights, dtype=np.float64)
            weight_dict[ts_id] = ts_weights

        if self.use_smoothing:
            weight_factor_dict = self.smooth(weight_dict)
        else:
            weight_factor_dict = self.normalize(weight_dict)

        gc.collect()
        return weight_factor_dict

    def get_temporal_difference(self):

        """
        Build the temporal differences matrix/dictionary between each timeseries and the rest of timeseries.
        Temporal difference is a absolute value difference between two timestamps of two given timeseries.

        e.g. TS1=[1, 2, 3], TS2=[1, 1, 2]
        # For L1 or L2 --> temporal difference TD = [0, 1, 1]
        # For DTW, CorT or CorT_DTW, then it depends on the mapping/path.

        :return: a dictionary of ts_id --> temporal differences
        """

        if self.md_matrix.distance_measure in ['l1', 'l2']:
            weight_dict = {i: [] for i in range(self.data_length)}
            for ts_1, ts_2 in itertools.combinations_with_replacement(range(self.data_length), 2):
                if ts_1 == ts_2:
                    weight_dict[ts_1].append(np.zeros(self.learningset[ts_1].data[self.md_matrix.var_id].length))
                else:
                    weight_dict[ts_1].append(self.md_matrix.paths[(ts_1, ts_2)])
                    weight_dict[ts_2].append(self.md_matrix.paths[(ts_1, ts_2)])

            gc.collect()
            return {i: np.array(weight_dict[i]) for i in weight_dict}
        else:

            # If the distance measure is DTW, CorT or CorT_DTW, then parse the path between each pair of timeseries,
            # and update the temporal differences
            weight_dict = dict()
            for ts_id in range(self.data_length):
                ts_length = self.learningset[ts_id].data[self.md_matrix.var_id].length
                weight_dict[ts_id] = [{i: [] for i in self.learningset.labels_set} for _ in range(ts_length)]
                for elem in weight_dict[ts_id]:
                    elem[self.learningset[ts_id].label].append(0)
            for ts_1, ts_2 in itertools.combinations(range(self.data_length), 2):
                ts_1_weights = weight_dict[ts_1]
                ts_2_weights = weight_dict[ts_2]
                path = self.md_matrix.paths[(ts_1, ts_2)]
                for step in path:
                    i = int(step[0])
                    j = int(step[1])
                    dist_value = step[2]
                    ts_1_weights[i][self.learningset[ts_2].label].append(dist_value)
                    ts_2_weights[j][self.learningset[ts_1].label].append(dist_value)
            gc.collect()
            return weight_dict

    def smooth(self, weight_dict):

        """
        Smooth an w_vector with a sliding window (back and forth).
        :param weight_dict: Array of weight vectors to smooth
        """

        precision = 0.000000000001
        weight_factor_dict = dict()
        for factor in self.weight_factor:
            temp_dict = copy.deepcopy(weight_dict)
            for ts_id in temp_dict:
                temp_dict[ts_id] = np.power(temp_dict[ts_id], self.weight_factor[factor])
                w_vector = temp_dict[ts_id]
                copy_vector = np.zeros(len(w_vector))
                ts_length = len(w_vector)
                ts_window = round(ts_length * (self.smoothing_window / 100))
                if ts_window < 1:
                    ts_window = 1
                elif ts_window > 10:
                    ts_window = 10

                cumulative_weight = sum(w_vector[0:ts_window + 1])
                current_index = ts_window + 1
                copy_vector[0] = cumulative_weight / current_index
                min_weight, max_weight = copy_vector[0], copy_vector[0]
                data_length = len(w_vector)
                for i in range(1, ts_window + 1):
                    cumulative_weight += w_vector[current_index]
                    current_index += 1
                    copy_vector[i] = cumulative_weight / current_index
                    if copy_vector[i] < min_weight:
                        min_weight = copy_vector[i]
                    if copy_vector[i] > max_weight:
                        max_weight = copy_vector[i]

                first_index = 0
                nb_elements = (ts_window * 2) + 1

                for i in range(ts_window + 1, data_length - ts_window):
                    cumulative_weight -= w_vector[first_index]
                    cumulative_weight += w_vector[current_index]
                    first_index += 1
                    current_index += 1
                    copy_vector[i] = cumulative_weight / nb_elements
                    if copy_vector[i] < min_weight:
                        min_weight = copy_vector[i]
                    if copy_vector[i] > max_weight:
                        max_weight = copy_vector[i]

                for i in range(data_length - ts_window, data_length):
                    cumulative_weight -= w_vector[first_index]
                    first_index += 1
                    nb_elements -= 1
                    copy_vector[i] = cumulative_weight / nb_elements
                    if copy_vector[i] < min_weight:
                        min_weight = copy_vector[i]
                    if copy_vector[i] > max_weight:
                        max_weight = copy_vector[i]

                if self.discretize:
                    disc_step = (max_weight + precision - min_weight) / self.bins
                    bins = np.arange(min_weight, max_weight, disc_step)
                    copy_vector = np.digitize(copy_vector, bins)
                vector_sum = np.sum(copy_vector)
                copy_vector = np.divide(copy_vector, vector_sum)
                temp_dict[ts_id] = Wo.WeightObject(data=copy_vector, unnormalized_sum=vector_sum)
            weight_factor_dict[factor] = temp_dict
        return weight_factor_dict

    def normalize(self, weight_dict):

        """
        Normalizing a set of weight vectors (sum equal 1)
        :param weight_dict: Array of weight vectors to normalize
        """

        weight_factor_dict = dict()
        for factor in self.weight_factor:
            for ts_id in weight_dict:
                weight_vector = np.power(weight_dict[ts_id], self.weight_factor[factor])
                vector_sum = np.sum(weight_vector)
                weight_vector = np.divide(weight_vector, vector_sum)
                weight_dict[ts_id] = Wo.WeightObject(data=weight_vector, unnormalized_sum=vector_sum)
            weight_factor_dict[factor] = weight_dict
        return weight_factor_dict
