import numpy as np
import tdt.decisiontree.core.GainObject as Go


"""
The LearningSet class mimics a csv dataset where the first row in the csv is mapped to
an array of Variable, and the rest of the rows are mapped to an array of Observation.
The timestamp feature is to map a timeseries to timestamp, but since many timeseries could share
one timestamp array, we need a dictionary to store this information.

e.g. having a csv dataset:

|   First_name    | Last_name     | Age   | Travel_history          |
|   name_1        | last_name_1   | age_1 | path_to_timeseries_1    |
|   name_2        | last_name_2   | age_2 | path_to_timeseries_2    |
|   name_3        | last_name_3   | age_3 | path_to_timeseries_3    |

and consider path_to_timesries_1 has the following:

|   Date    |   Destination |
|   1990    |   LA          |
|   1992    |   Paris       |
...
|   2000    |   Grenoble    |

Then parsing the csv file using the LearningSetBuilder will give us the following LearningSet object ls:

ls.name='LearningSet'
ls.variables = [Variable(name='first_name', v_type='string', imp_type=NOMINAL),
                Variable(name='last_name', v_type='string', imp_type=NOMINAL),
                Variable(name='Age', v_type='int', imp_type=CONTINUOUS),
                Variable(name='Travel_history-Date', v_type='int', imp_type=TIMESTAMP),
                Variable(name='Travel_history-Destination', v_type='float', imp_type=TIMESERIES)]

ls.observations =   [Observations(id=1, data=[name_1, last_name_1, age_1, 
                            [1990, 1992, ..., 2000], ['LA', 'Paris', ..., 'Grenoble']]),
                     Observations(id=1, data=[name_1, last_name_1, age_1, ...]),
                     Observations(id=1, data=[name_1, last_name_1, age_1, ...])]
                     

In the previous structure, the index of the timestamp Travel_history-Data is 3
and the related timeseries is Travel_history-Destination is 4, hence:

ls.timestamp = [(3, 4)], in this way, we know that the timeseries Destination is related to the timestamp Date.
"""


class LearningSet(object):
    """
    The LearningSet class stores the raw data, the variables information, timestamp relations,
    and some other needed meta-data.
    To build a LearningSet, check the LearningSetBuilder class
    """

    def __init__(self, name='LearningSet',
                 observations=None,
                 variables=None,
                 meta_data=None,
                 timestamp=None):

        """
        :param name: name of the learningset
        :param observations: an array of Type Observation (raw data)
        :param variables:  an array of Type Variable
        :param meta_data: a dictionary of information
        :param timestamp: An array of tuple (Timestamp_index, Timeseries_index), since two timeseries might
        share a timestamp.
        """
        self._name = name
        self._observations = observations
        self._variables = variables
        self._meta_data = meta_data
        self._timestamp = timestamp

        # Internal use
        # This mapper maps name of a variable by its index.
        # This is needed for example in predicting an observation using decision tree.
        self._var_ind_mapper = dict()
        for i in range(len(variables)):
            self._var_ind_mapper[variables[i].name] = i

        if observations[0].label is not None:
            temp_labels = []
            # Observation Ids are string, but for better performance, we save a map between the obs_id and the index
            for i in range(len(observations)):
                temp_labels.append(observations[i].label)

            # Internal copy for the ease of use of labels
            self._labels = np.array(temp_labels)
            self._labels_set = set(temp_labels)

    # Getters and Setters
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def observations(self):
        return self._observations

    @observations.setter
    def observations(self, value):
        self._observations = value

    @property
    def variables(self):
        return self._variables

    @variables.setter
    def variables(self, value):
        self._variables = value

    @property
    def meta_data(self):
        return self._meta_data

    @meta_data.setter
    def meta_data(self, value):
        self._meta_data = value

    @property
    def timestamp(self):
        return self._timestamp

    @timestamp.setter
    def timestamp(self, value):
        self._timestamp = value

    @property
    def var_ind_mapper(self):
        return self._var_ind_mapper

    @property
    def labels_set(self):
        return self._labels_set

    @labels_set.setter
    def labels_set(self, value):
        self._labels_set = value

    # Functionality
    def data(self, vars_ids, keys=None, sklearn=False):
        # Getter of the data for a subset of observations

        # build a matrix for sklearn (multi-variables)
        if sklearn:
            if keys is None:
                return [o.data[vars_ids] for o in self.observations]
            else:
                return [o.data[vars_ids] for o in self.observations[keys]]

        # Return an array of GainObject (one variable) that is used
        # to calculate the best split in the decision tree.
        else:
            obs_enumerations = enumerate(self.observations)
            variable = self._variables[vars_ids]
            if variable.imp_type.value == 'timeseries':
                if keys is None:
                    return [Go.GainObject(obs_index=ind, label=o.label)
                            for ind, o in obs_enumerations]
                else:
                    return [Go.GainObject(obs_index=ind, label=o.label)
                            for ind, o in obs_enumerations if ind in keys]
            else:
                if keys is None:
                    return [Go.GainObject(obs_index=ind, label=o.label, distance=o.data[vars_ids])
                            for ind, o in obs_enumerations]
                else:
                    return [Go.GainObject(obs_index=ind, label=o.label, distance=o.data[vars_ids])
                            for ind, o in obs_enumerations if ind in keys]

    def labels(self, keys=None):
        # Getter of the labels for a subset of observations

        if keys is None:
            return self._labels
        else:
            return self._labels[keys]

    def __len__(self):
        return len(self._observations)

    def count_lb_stats(self, keys=None):
        # Get the labels Counter for a subset of observations

        if keys is None:
            temp_labels = self._labels
        else:
            temp_labels = self._labels[keys]
        return dict(zip(*np.unique(temp_labels, return_counts=True)))

    def get_obs_label(self, obs_index):
        # Get the label for a specific obs_id

        return self._observations[obs_index].label

    def get_obs_data(self, obs_index):
        # Get the data for a specific obs_index

        return self._observations[obs_index].data

    def get_labels_by_indexes(self, obs_indexes):
        # Get the different labels for a subset of observations

        labels = set()
        for i in obs_indexes:
            labels.add(self._observations[i].label)
        return labels

    def get_observations_ids(self, indexes):

        return {o.obs_id for ind, o in enumerate(self._observations) if ind in indexes}

    def __getitem__(self, idx):
        return self._observations[idx]
