import math
import random
import re
import numpy as np
import tdt.distances.distances as dist
import tdt.distances.DistanceObject as Do

"""
This file contains the implementation of some stand-alone utility functions,
the functions will not be related to any algorithm, but rather used by that algorithm.
"""


def get_distance(x=None, y=None, w=None, measure=None, window=None,
                 get_path=False, path=None, switch_path=False):

    """
    Compute the distance between two numpy array using one of the following measures:
    [L1, L2, DTW, Cort, Cort_DTW] with their variation (weighted or non-weighted)

    :param x: an np array of float values
    :param y: an np array of float values
    :param w: an np array of float values (weights)
    :param measure: the measure to calculate
    :param window: window look-ahead for DTW, CorT and CorT_DTW
    :param get_path: Return the path or not (DTW, CorT, CorT_DTW)
    :param path: use an already calculated path in the distance computation
    :param switch_path: switch each pair of the path (i, j) --> (j, i)
    :return: the distance value
    """

    if measure in ['l1', 'l2']:
        return known_measures[measure](x, y, w, get_path)

    new_path = None
    if path is not None:
        new_path = path[:, [1, 2]] if switch_path else path[:, [0, 2]]

    return known_measures[measure](x, y, w, get_path, new_path, window)


def norm_l1(x, y, w=None, get_path=False):

    """
    :param x: an np array of float values (first timeseries)
    :param y: an np array of float values (second timeseries)
    :param w: an np array of float values (weights)
    :param get_path: Return the array of temporal distances between x and y
    :return: the distance value
    """

    dist_obj = Do.DistanceObject()
    if get_path:
        dist_obj.path = np.fabs(x - y)

    if w is None:
        dist_obj.additive_dist = np.linalg.norm(x - y, 1)
    else:
        d = x - y
        dist_obj.additive_dist = np.linalg.norm(w * d, 1)
    return dist_obj


def norm_l2(x, y, w=None, get_path=False):

    """
    :param x: an np array of float values (first timeseries)
    :param y: an np array of float values (second timeseries)
    :param w: an np array of float values (weights)
    :param get_path: Return the array of temporal distances between x and y
    :return: the distance value
    """

    dist_obj = Do.DistanceObject()
    if get_path:
        dist_obj.path = np.fabs(x - y)

    if w is None:
        dist_obj.additive_dist = np.linalg.norm(x - y, 2)
    else:
        d = x - y
        dist_obj.additive_dist = np.sqrt((w * d * d).sum())
    return dist_obj


def dtw(x, y, w=None, get_path=False, path=None, window=None):

    """
    :param x: an np array of float values (first timeseries)
    :param y: an np array of float values (second timeseries)
    :param w: an np array of float values (weights)
    :param get_path: Return the path or not (DTW, CORT, CORT_DTW)
    :param path: Given path to be used in the distance computation
    :param window: window look-ahead for DTW, CorT and CorT_DTW
    :return: the distance value
    """

    return dist.dtw(x, y, weight=w, get_path=get_path, path=path, window=window)


def cort(x, y, w=None, get_path=False, path=None, window=None):

    """
    :param x: an np array of float values (first timeseries)
    :param y: an np array of float values (second timeseries)
    :param w: an np array of float values (weights)
    :param get_path: Return the path or not (DTW, CORT, CORT_DTW)
    :param path: Given path to be used in the distance computation
    :param window: window look-ahead for DTW, CorT and CorT_DTW
    :return: the distance value
    """

    return dist.cort(x, y, weight=w, get_path=get_path, path=path, window=window)


def cort_dtw(x, y, w=None, get_path=False, path=None, window=None):

    """
    :param x: an np array of float values (first timeseries)
    :param y: an np array of float values (second timeseries)
    :param w: an np array of float values (weights)
    :param get_path: Return the path or not (DTW, CORT, CORT_DTW)
    :param path: Given path to be used in the distance computation
    :param window: window look-ahead for DTW, CorT and CorT_DTW
    :return: the distance value
    """

    return dist.cort_dtw(x, y, weight=w, get_path=get_path, path=path, window=window)


def get_gain(gain, probability=1, stats=None, stats_weights=None):

    """
    Calculate the gain (Gini, Entropy).

    :param gain: type of gain to calculate (gini, entropy)
    :param probability: probability of the distribution
    :param stats: Counter for the labels
    :param stats_weights: weights relative to each stats (unbalanced distribution)
    :return: the gain value
    """

    if gain == 'gini':
        return compute_gini(probability, stats, stats_weights)
    return compute_entropy(probability, stats, stats_weights)


def compute_gini(probability=1.0, stats=None, stats_weights=None):

    """
    Compute the gain-gini value

    :param probability: Probability of the distribution
    :param stats: Counter of the labels
    :param stats_weights: weights relative to each stats (unbalanced distribution)
    :return: true_gain, node_gain (without taking into account the probability)
    """

    if probability == 0.0:
        return 0.0, 0.0

    _cc = len(stats)
    if _cc == 0:
        return 0.0, 0.0

    occurrences = np.array([float(stats[label] * stats_weights[label]) for label in stats])
    sums = np.sum(occurrences)
    node_gain = (1 - (np.sum(occurrences * occurrences) / (sums * sums)))
    true_gain = probability * node_gain

    return true_gain, node_gain


def compute_entropy(probability=1.0, stats=None, stats_weights=None):

    """
    Compute the gain-entropy value

    :param probability: Probability of the distribution
    :param stats: Counter of the labels
    :param stats_weights: weights relative to each stats (unbalanced distribution)
    :return: true_gain, node_gain (without taking into account the probability)
    """

    if probability == 0.0:
        return 0.0, 0.0
    values = np.array([float(stats[label] * stats_weights[label]) for label in stats])
    sums = np.sum(values)
    entropy = 0
    for val in values:
        if val == 0:
            continue
        division = val / sums
        entropy += -1.0 * division * math.log2(division)
    gain = probability * entropy

    return gain, entropy


def check_condition(value_to_check, original_value, condition):

    """
    Used when we predict using TDT
    :param value_to_check: the value to check against a condition
    :param original_value: the original value
    :param condition: a string representation of a condition
    :return: The condition result
    """

    if condition == '=':
        return value_to_check == original_value
    if condition == '!=':
        return value_to_check != original_value
    if condition == '<=':
        return value_to_check <= original_value
    if condition == '<':
        return value_to_check < original_value
    if condition == '>=':
        return value_to_check >= original_value
    if condition == '>':
        return value_to_check > original_value


def random_ref_generator(l_k_dict, nb_refs=None):

    """
    Choosing randomly a set of keys (references) to build the tree from.
    The number of keys are relative to the total number of available keys,
    and it is indicated by LOG2(N) ± nb_available_labels

    :param l_k_dict: label--> set(obs_ids)
    :param nb_refs: number of references
    :return: set of keys
    """

    nb_keys = sum(len(l_k_dict[l]) for l in l_k_dict)

    if nb_refs is None:
        nb_refs = int(math.floor(math.log2(nb_keys)))
    random_keys = set()
    chosen_keys = set()

    for label in l_k_dict:
        l_k_len = len(l_k_dict[label])
        nb_to_choose = (l_k_len / nb_keys) * nb_refs
        if nb_to_choose < 1:
            nb_to_choose = 1
        else:
            nb_to_choose = math.floor(nb_to_choose)
        temp_ch_keys = set(random.sample(l_k_dict[label], nb_to_choose))
        chosen_keys.update(temp_ch_keys)
        random_keys.update(l_k_dict[label] - temp_ch_keys)

    total_chosen = len(chosen_keys)
    if len(chosen_keys) < nb_refs:
        chosen_keys.update(random.sample(random_keys, nb_refs - total_chosen))

    return chosen_keys


def pattern_to_re(pattern):
    flag = False
    len_flag = True
    res = ''
    length = 0
    for c in pattern:
        if c == ']':
            res += c
            len_flag = True
            continue
        if flag:
            res += c + '-'
            length += 1
            flag = False
            continue
        if c == '[':
            flag = True
            len_flag = False
            res += c
            continue
        if len_flag:
            length += 1
        res += c
    return res, length


def update_lower_bound(criteria, original_val, update_value, data_length=1):

    if criteria == 'norm':
        return original_val - update_value

    return original_val / math.sqrt(data_length) - update_value


def update_upper_bound(criteria, original_val, update_value, data_length=1):
    if criteria == 'norm':
        return original_val + update_value

    return (original_val + update_value) * math.sqrt(data_length)


def rand_ref_generator(data, nb_refs, label_stats):

    """
    Choosing randomly a set of keys (references) to build the tree from.
    The number of keys are relative to the total number of available keys,
    and it is indicated by LOG2(N) ± nb_available_labels

    :param data: array of GainObject
    :param nb_refs: number of references
    :param label_stats: label--> set(obs_ids)
    :return: set of keys
    """

    nb_keys = len(data)

    if nb_refs is None:
        nb_refs = int(math.floor(math.log2(nb_keys)))
    random_keys = list(range(nb_keys))
    random.shuffle(random_keys)
    lab_nb_keys = dict()

    for label in label_stats:
        l_k_len = label_stats[label]
        nb_to_choose = (l_k_len / nb_keys) * nb_refs
        if nb_to_choose < 1:
            nb_to_choose = 1
        else:
            nb_to_choose = math.floor(nb_to_choose)
        lab_nb_keys[label] = nb_to_choose

    count = len(label_stats)
    chosen_keys = []
    for key in random_keys:
        key_label = data[key].label
        if lab_nb_keys[key_label] > 1:
            chosen_keys.append(key)
            lab_nb_keys[key_label] -= 1
        elif lab_nb_keys[key_label] == 1:
            chosen_keys.append(key)
            lab_nb_keys[key_label] -= 1
            count -= 1
        if count == 0:
            break

    return chosen_keys


def initializing_nr(nb_examples, init_func_id):

    """
    Get the number of initial references for percentage based search

    :param nb_examples: Total number of examples in a node
    :param init_func_id: function id (1=Log(N), 2=N^(1/5), 3=N^(1/2.5), 4=sqrt(N), 5=N^(1/1.6)
    :return: Number of initial references (int)
    """

    if init_func_id == 1:
        return math.ceil((math.floor(math.log2(nb_examples))))

    if init_func_id == 2:
        return math.ceil(nb_examples**(1/5))

    if init_func_id == 3:
        return math.ceil(nb_examples ** (1 / 2.5))

    if init_func_id == 4:
        return math.ceil(math.sqrt(nb_examples))

    return math.ceil(nb_examples ** (1 / 1.6))


def discretize_ts(ts, breakpoints):
    """
    Discretize a given time series according to its breakpoints.
    :param ts: the input timeseries
    :type ts: TimeSeries
    """

    digitized_data = np.digitize(ts, breakpoints[0])
    data_to_symbols = "".join([to_chr(int(x) - 1) for x in digitized_data])
    return data_to_symbols


def to_chr(x):
    """
    Converts an int to a char. If the int is larger than 25
    we switch to the lower version of the char.
    :param x: a discrete value (typically small)
    :type x: a positive integer
    :return: a character representing the integer
    :rtype: string (as return by chr)
    """
    # 71 = 97-26, 91 ascii number of "a", 26 letters
    return chr(x+71) if x > 25 else chr(x+65)


def pattern_matcher(pattern, length, string):
    index = -1
    positions = []
    while len(string) >= length:
        res = re.search(pattern, string)
        if res:
            index += res.start() + 1
            positions.append(index)
            string = string[res.start() + 1:]
        else:
            break
    return positions


def pattern_to_re3(pattern):
    _bool = False
    _res = ''
    for c in pattern:
        if _bool:
            _res += c + '-'
            _bool = False
            continue
        if c == '[':
            _bool = True
        _res += c
    return _res


known_measures = {'l1': norm_l1, 'L1': norm_l1,
                  'l2': norm_l2, 'L2': norm_l2,
                  'dtw': dtw, 'DTW': dtw,
                  'cort': cort, 'CORT': cort,
                  'cort_dtw': cort_dtw, 'CORT_DTW': cort_dtw}

