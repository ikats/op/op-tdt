import collections
import tdt.decisiontree.core.DecisionTree as Dt
import tdt.decisiontree.core.DecisionTreeClassifier as Dtc
import tdt.decisiontree.core.LearningSetBuilder as Lsb
import tdt.decisiontree.core.CrossValidation as Cv
from ikats.core.resource.api import IkatsApi as Api

# Import logger
import logging

# Logger definition
LOGGER = logging.getLogger(__name__)


def tdt_run(learningset=None,
            rhm_patterns=None,
            split_criteria='gini',
            min_gain_percentage=0.0,
            min_observations_node=2,
            majority_class_percentage=0.98,
            max_depth=None,
            ts_splitter=2,
            distance_measure='l1',
            method='add',
            weight_type='uniform',
            weight_factor='normal',
            window=None,
            store_path=True,
            cv=None,
            search_percentage=1.0,
            privacy=True):

    if not learningset:
        raise IOError('No input learning set was given')

    # Parameters check
    distance_measure = [v.strip() for v in distance_measure.split(',')]
    method = [v.strip() for v in method.split(',')]
    weight_type = [v.strip() for v in weight_type.split(',')]
    weight_factor = [v.strip() for v in weight_factor.split(',')]

    tree_classifier = Dtc.DecisionTreeClassifier(min_gain_percentage=float(min_gain_percentage),
                                                 split_criteria=split_criteria,
                                                 distance_measure=distance_measure,
                                                 method=method,
                                                 weight_type=weight_type,
                                                 weight_factor=weight_factor,
                                                 store_path=store_path,
                                                 max_depth=max_depth,
                                                 ts_splitter=ts_splitter,
                                                 builder='IKATS',
                                                 source='IKATS',
                                                 min_observations_node=min_observations_node,
                                                 majority_class_percentage=majority_class_percentage,
                                                 window=window)
    ls_builder = Lsb.LearningSetBuilder()

    try:
        ls = ls_builder.build(ls_format='ikats', ls_name=learningset, rhm_patterns=rhm_patterns,
                              obs_prefix='obs_train_', ts_prefix='TS_train_')
    except IOError as _:
        raise IOError('Error reading the dataset')

    if cv == 1:
        cv = None

    if cv:
        try:
            tree = Cv.train_resampling(classifier=tree_classifier, learningset=ls, k=cv)
        except RuntimeError as _:
            raise RuntimeError('Error occurred in cross-validation, please check the parameters and try again!')
    else:

        try:
            tree = tree_classifier.fit(ls)
        except RuntimeError as _:
            raise RuntimeError('Error occurred while building the tree, please check the parameters and try again!')

    tdt_res = collections.OrderedDict()
    tdt_res['header'] = tree.header
    tdt_res['tree'] = tree.tree
    tdt_res['validation'] = tree.validation
    tdt_res['data'] = tree.data
    LOGGER.debug('TDT finished successfully')
    return tdt_res


def tdt_test(tdt_model=None,
                testset=None,
                rhm_patterns=None,
                privacy=True):

    if not tdt_model:
        raise IOError('No tree was given!')

    if not testset:
        raise IOError('No testset was given!')

    ls_builder = Lsb.LearningSetBuilder()
    try:
        ts = ls_builder.build(ls_format='ikats_test', ls_name=testset, rhm_patterns=rhm_patterns,
                              obs_prefix='obs_test_', ts_prefix='TS_test_')
    except IOError as _:
        raise IOError('Error reading the dataset')

    tree = Dt.DecisionTree()
    tree.parse(tdt_model)

    try:
        tree.predict(ts)

    except RuntimeError as _:
        LOGGER.error('Error occurred while testing, the tree is returned without validation')

    tdt_res = collections.OrderedDict()
    tdt_res['header'] = tree.header
    tdt_res['tree'] = tree.tree
    tdt_res['validation'] = tree.validation
    tdt_res['data'] = tree.data
    LOGGER.debug('TDT finished successfully')
    return tdt_res, str(tdt_res['validation']['confusion-matrix'])


def tdt_predict(tdt_model=None,
                testset=None,
                rhm_patterns=None,
                privacy=True):
    if not tdt_model:
        raise IOError('No tree was given!')

    if not testset:
        raise IOError('No testset was given!')

    ls_builder = Lsb.LearningSetBuilder()
    try:
        ts = ls_builder.build(ls_format='ikats_predict', ls_name=testset, rhm_patterns=rhm_patterns,
                              obs_prefix='obs_predict_', ts_prefix='TS_predict_')
    except IOError as _:
        raise IOError('Error reading the dataset')

    tree = Dt.DecisionTree()
    tree.parse(tdt_model)
    new_set = Api.table.read(testset)

    try:
        predictions = tree.true_predict(ts)
        new_set["headers"]["col"]["data"].append('target')
        for ind, p_label in enumerate(predictions):
            new_set["content"]["cells"][ind].append(p_label)

    except RuntimeError as _:
        LOGGER.error('Error occurred while testing, the tree is returned without validation')

    tdt_res = collections.OrderedDict()
    tdt_res['header'] = tree.header
    tdt_res['tree'] = tree.tree
    tdt_res['validation'] = tree.validation
    tdt_res['data'] = tree.data
    LOGGER.debug('TDT finished successfully')
    return tdt_res, new_set
