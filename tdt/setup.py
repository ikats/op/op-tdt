from setuptools import setup, find_packages, Extension

ext_modules = [Extension(name="distances.distances",
                         sources=["distances/cdtw_mini.c"],
                         depends=["distances/cdtw.h"])]

print(find_packages())
setup(name='tdt',
      version='0.1',
      description='TDT algorithm',
      url='https://gitlab.com/sami.kh/Ikats-Portable-Algo.git',
      author='Sami ALKHOURY',
      author_email='sami.alkhoury@univ-grenoble-alpes.fr',
      license='MIT',
      packages=find_packages(),
      setup_requires=[],
      include_package_data=True,
      ext_modules=ext_modules,
      install_requires=["numpy", "sklearn", "scipy", "cffi"],
      scripts=['tdt_train', 'tdt_test'],
      zip_safe=False)
