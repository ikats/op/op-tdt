## TDT Operator User Manual
Date: 5th September 2018
Revision: V 0.25
Author: S. Alkhoury, G. Bisson
Project: IKATS

### 1. Introduction

This document is a description on how to use the operator TDT (Temporal Decision Tree) with the presentation of the its various parameters.

The program directory is structured in the following way:
> tdt/
> - decisiontree/
> - distances/
> - main.py

- decisiontree: contains the implementation of the TDT
- distances: contains the C implementation of DTW, CorT and CorT_DTW and their python wrapper.
- main.py: entry point for IKATS (tdt_run)

### 2. The Temporal Decision Tree Operator

TDT is a classification algorithm that builds a decision tree on datasets of timeseries (sequences). On each node, the tree select the best timeseries that represent their relative class. The tree can select one (hypersphere) or two (hyperplane) timeseries in each node. In case of hypersphere, the tree tries to separate one class from the others and in hyperplane the tree separate two classes from each other.

The tree picks the timeseries with the best information gain (gini / entropy) to construct the node.

In the next section, a brief description of the various inputs/parameters that the tree uses.

##### 2.1 Input

The operators accepts several arguments:

- **learningset**: The learningset to build the tree upon (table structure of IKATS)
- **rhm_patterns**: Patterns dictionary learned from RHM

##### 2.2 Parameter list

- ***minimum_gain_percentage***: a stopping criteria for the tree. The tree will stop exploring nodes if the gain value is less or equal to the given value.
- ***split_criteria***: whether to use gini or entropy
- ***distance_measure***: the tree implements various distance measure to calculate the similarity between two timeseries {l1: Manhattan, l2: Euclidean, dtw, cort, cort_dtw}
- ***method***: for dtw, cort and cort_dtw, whether to use an additive distance or averaged one.
- ***weight_type***: the tree also implements a weighting method for the timeseries. the weights reflect the importance of timestamps of each timeseires separately, the higher the value, the more importance the timestamp for the relative timeseries. Currently, the tree implements only the *Between/Withing* approach. (for more information about the method, check the weights.pdf file)
- ***weight_factor***: a factor for the weights (normal, square, sqrt)
- ***min_observation_node***: minimum number of observations per node (stopping criteria)
- ***majority_class_percentage***: if one of the classes has a certain percentage, the tree will stop exploring the nodes. (a stopping criteria)
- ***max_depth***: maximum depth of the tree (a stopping criteria)
- ***window***: for dtw, cort and cort_dtw, it reflects the number of look-ahead time-stamps the distance measure uses.
- ***store_path***: if the path of non-weighted distances will be used in weighted distances.
- ***unbalanced_tree***: if the class distribution is balanced or not.
- ***cv***: number of folds for the cross-validation
- ***search_percentage***: search a percentage of the full space of timeseries
- **privacy**: whether to out all information or only information needed for visualization

In the following, a table that contains the parameters and their default and possible values:

| 	Key 						| Default value 	|  Possible values 
| ------------------------------------	| --------------------- 	| 	------------- 	
| majority_class_percentage	| 	0.98		| 	float		
| min_gain_percentage		| 	0.0			| 	float 	
| distance_measure			| 	l2			| 	{l1, l2, dtw, cort, cort_dtw}
| split_criteria				|  	2			| 	{1: Spherical, 2:Spherical/Hyperplane, 3:Hyperplane}
| method					|   	add			|	{add, avg}
| weight_type				|   	uniform		|	{uniform, bw}
| weight_factor	    			|   	normal		|	{normal, square, sqrt}
| min_observation_node		|   	2			|	integer
| max_depth					|   	max		|	integer [1-max]
| window    					|   	Null    		|   	integer [1-max] 
| store_path					|   	1			|	(1: True, 0: False)
| unbalanced_tree			|   	0			|	(1: unbalanced, 0:balanced)
| cv							| 	null			| 	integer
| search_percentage			|	1			|	[minimum possible value, 1]
| privacy					|	True		|	{True, False}



**2.3 Output**

The output of the algorithm is a json structure that contains all information needed to test the tree on a new dataset and various different information. The full description can be found in the file ***tdt_output_format.pdf***



### 3. The TDT Test Operator

An operator that is used to test a TDT on a new dataset.

##### 3.1 Input

The operator accepts several arguments:

- **tdt_model**: The tdt model to be used in testing
- **testset**: a new dataset for testing
- **rhm_patterns**: Patterns dictionary learned from RHM

##### 3.2 Parameter list

- **privacy**: whether to out all information or only information needed for visualization

In the following, a table that contains the parameters and their default and possible values:

| 	Key 						| Default value 	|  Possible values 
| ------------------------------------	| --------------------- 	| 	------------- 	
| privacy					|	True		|	{True, False}



**3.3 Output**

- **TDT**: Json representation of the tree that contains information needed for visualization, accuracy, confusion-matrix and traces
- **cm**: confusion-matrix of the testset



### 4. The TDT Predict Operator

An operator that is used to test a TDT on a new dataset.

##### 4.1 Input

The operator accepts several arguments:

- **tdt_model**: The tdt model to be used in testing
- **testset**: a new dataset for testing
- **rhm_patterns**: Patterns dictionary learned from RHM

##### 4.2 Parameter list

- **privacy**: whether to out all information or only information needed for visualization

In the following, a table that contains the parameters and their default and possible values:

| 	Key 						| Default value 	|  Possible values 
| ------------------------------------	| --------------------- 	| 	------------- 	
| privacy					|	True		|	{True, False}



**4.3 Output**

Same as the TDT operator, but the output will contain information about the testing:

- **TDT**: Json representation of the tree that contains information needed for visualization, accuracy, confusion-matrix and traces
- **new_table**: An ikats table that contains the predictions of the tree.



###### *Note:*

- CorT and CorT_DTW has no weighted distances implementation (Only uniform with both variation, additive and average)
- 'uniform' distances are always calculated since its path/mapping is used to compute the weights. 
- Including 'uniform' in 'weight-type' means that they will be used to generate the best tree.
- the search_percentage is not yet supported in this version (to be added soon)


**IKATS API:**

- The algorithm only uses the ***ikats.core.resource.api*** to read timeseries in IKATS framework.
- For reading IKAST table format, ikats.table.read was also used.